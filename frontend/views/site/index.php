<?php

/* @var $this yii\web\View */

?>
<!-- ***** Welcome Area Start ***** -->
<div class="welcome-area" id="welcome">
	<!-- ***** Header Text Start ***** -->
	<div class="header-text">
		<div class="container">
			<div class="row">
				<div class="offset-xl-3 col-xl-6 offset-lg-2 col-lg-8 col-md-12 col-sm-12">
					<h1>Парсинг <strong>Авито</strong> и лучшие <strong>данные</strong> для развития Вашего <strong>бизнеса</strong></h1>
					<p><strong>T-Gate Soft</strong> - это профессиональная команда, способная решать широкий спектр задач, используя самые <strong>современные технологии</strong>.</p>
					<br>
				</div>
			</div>
		</div>
	</div>
	<!-- ***** Header Text End ***** -->
</div>
<!-- ***** Welcome Area End ***** -->

<!-- ***** Features Small Start ***** -->
<section class="section home-feature">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="row">
					<!-- ***** Features Small Item Start ***** -->
					<div class="col-lg-4 col-md-6 col-sm-6 col-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.2s">
						<div class="features-small-item">
							<div class="icon">
								<i><img src="/images/featured-item-01.png" alt=""></i>
							</div>
							<h5 class="features-title">Выгрузка с Авито</h5>
							<p>Выгрузка данных с крупнейшей площадки обьявлений, включая номера телефонов</p>
						</div>
					</div>
					<!-- ***** Features Small Item End ***** -->
					<!-- ***** Features Small Item Start ***** -->
					<div class="col-lg-4 col-md-6 col-sm-6 col-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.4s">
						<div class="features-small-item">
							<div class="icon">
								<i><img src="/images/featured-item-01.png" alt=""></i>
							</div>
							<h5 class="features-title">Круглосуточная поддержка</h5>
							<p>Незамедлительно связавайтесь с нами если у вас возникнет проблема или появится новая идея</p>
						</div>
					</div>
					<!-- ***** Features Small Item End ***** -->
					<!-- ***** Features Small Item Start ***** -->
					<div class="col-lg-4 col-md-6 col-sm-6 col-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.6s">
						<div class="features-small-item">
							<div class="icon">
								<i><img src="/images/featured-item-01.png" alt=""></i>
							</div>
							<h5 class="features-title">Личный кабинет</h5>
							<p>Планируйте и управляйте своими выгрузками в удобном личном кабинете </p>
						</div>
					</div>
					<!-- ***** Features Small Item End ***** -->
				</div>
			</div>
		</div>
	</div>
</section>
<!-- ***** Features Small End ***** -->
<!-- ***** Features Big Item Start ***** -->
<section class="section padding-top-70 padding-bottom-0" id="features">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-12 col-sm-12 align-self-center" data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
				<img src="/images/left-image.png" class="rounded img-fluid d-block mx-auto" alt="App">
			</div>
			<div class="col-lg-1"></div>
			<div class="col-lg-6 col-md-12 col-sm-12 align-self-center mobile-top-fix">
				<div class="left-heading">
					<h2 class="section-title">Индивидуальный подход к каждому проекту</h2>
				</div>
				<div class="left-text">
					<p>Успех во многом зависит от того, насколько мы сконцентрированы на цели и как эффективно мы можем обсуждать ее. Общение с участниками команды позволяет нам своевременно выявлять любые несоответствия. Взаимодействуя с клиентами,
						мы можем быстро определять любые изменения в требованиях к проекту.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="hr"></div>
			</div>
		</div>
	</div>
</section>
<!-- ***** Features Big Item End ***** -->
<!-- ***** Features Big Item Start ***** -->
<section class="section padding-bottom-100">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-12 col-sm-12 align-self-center mobile-bottom-fix">
				<div class="left-heading">
					<h2 class="section-title">Помощь в интеграции</h2>
				</div>
				<div class="left-text">
					<p>Наши специалисты могут помочь Вам интегрировать выгрузку данных с вашим сайтом с помощью нашего API, которое предоставляется всем тарифным планам бесплатно. Так же мы всегда готовы разработать для Вас индивидуальное решение
						.</p>
				</div>
			</div>
			<div class="col-lg-1"></div>
			<div class="col-lg-5 col-md-12 col-sm-12 align-self-center mobile-bottom-fix-big" data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
				<img src="/images/right-image.png" class="rounded img-fluid d-block mx-auto" alt="App">
			</div>
		</div>
	</div>
</section>
<!-- ***** Features Big Item End ***** -->
<!-- ***** Home Parallax Start ***** -->
<section class="mini" id="work-process">
	<div class="mini-content">
		<div class="container">
			<div class="row">
				<div class="offset-lg-3 col-lg-6">
					<div class="info">
						<h2>Примеры</h2>
						<p>Мы предоставим выгрузку данных в любом, нужном вам формате.</p>
					</div>
				</div>
			</div>
			<!-- ***** Mini Box Start ***** -->
			<div class="row">
				<div class="col-lg-2 col-md-3 col-sm-6 col-6">

				</div>
				<div class="col-lg-2 col-md-3 col-sm-6 col-6">

				</div>
				<div class="col-lg-2 col-md-3 col-sm-6 col-6">
					<noindex>
						<a href="static/example.json" class="mini-box">
							<i><img width="50px" src="/assets/img/json.png" alt="JSON"></i>
							<strong>JSON</strong>
							<span></span>
						</a>
					</noindex>
				</div>
				<div class="col-lg-2 col-md-3 col-sm-6 col-6">
					<noindex>
						<a href="static/example.csv" class="mini-box">
							<i><img width="50px" src="/assets/img/csv.png" alt="CSV"></i>
							<strong>CSV</strong>
							<span></span>
						</a>
					</noindex>
				</div>

				<div class="col-lg-2 col-md-3 col-sm-6 col-6">

				</div>
			</div>
			<!-- ***** Mini Box End ***** -->
		</div>
	</div>
</section>
<!-- ***** Home Parallax End ***** -->
<!-- ***** Pricing Plans Start ***** -->
<section class="section colored" id="pricing-plans">
	<div class="container">
		<!-- ***** Section Title Start ***** -->
		<div class="row">
			<div class="col-lg-12">
				<div class="center-heading">
					<h2 class="section-title">Тарифные планы</h2>
				</div>
			</div>
			<div class="offset-lg-3 col-lg-6">
				<div class="center-text">
					<p>Если вы не нашли своего тарифного плана - напишите нам!</p>
				</div>
			</div>
		</div>
		<!-- ***** Section Title End ***** -->
		<div class="row">
			<!-- ***** Pricing Item Start ***** -->
			<div class="col-lg-4 col-md-6 col-sm-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.2s">
				<div class="pricing-item">
					<div class="pricing-header">
						<h3 class="pricing-title">Starter</h3>
					</div>
					<div class="pricing-body">
						<div class="price-wrapper">
							<span class="price">1000</span>
							<span class="currency">&#8381;</span>
							<span class="period">ежемесячно</span>
						</div>
						<ul class="list">
							<li class="active"><strong>500</strong> объявлений в день</li>
							<li class="active">личный кабинет</li>
							<li>номера телефонов</li>
							<li>парсинг по расписанию</li>
							<li>отправка файлов на почту</li>
						</ul>
					</div>
					<div class="pricing-footer">
						<a href="/internal/register/?t=1" class="main-button">Заказать</a>
					</div>
				</div>
			</div>
			<!-- ***** Pricing Item End ***** -->

			<!-- ***** Pricing Item Start ***** -->
			<div class="col-lg-4 col-md-6 col-sm-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.4s">
				<div class="pricing-item active">
					<div class="pricing-header">
						<h3 class="pricing-title">Premium</h3>
					</div>
					<div class="pricing-body">
						<div class="price-wrapper">
							<span class="price">3000</span>
							<span class="currency">&#8381;</span>
							<span class="period">ежемесячно</span>
						</div>
						<ul class="list">
							<li class="active"><strong>1000</strong> объявлений в день</li>
							<li class="active">личный кабинет</li>
							<li class="active">номера телефонов</li>
							<li>парсинг по расписанию</li>
							<li>отправка файлов на почту</li>
						</ul>
					</div>
					<div class="pricing-footer">
						<a href="/internal/register/?t=2" class="main-button">Заказать</a>
					</div>
				</div>
			</div>
			<!-- ***** Pricing Item End ***** -->

			<!-- ***** Pricing Item Start ***** -->
			<div class="col-lg-4 col-md-6 col-sm-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.6s">
				<div class="pricing-item">
					<div class="pricing-header">
						<h3 class="pricing-title">Advanced</h3>
					</div>
					<div class="pricing-body">
						<div class="price-wrapper">
							<span class="price">5000</span>
							<span class="currency">&#8381;</span>
							<span class="period">ежемесячно</span>
						</div>
						<ul class="list">
							<li class="active"><strong>3000</strong> объявлений в день</li>
							<li class="active">личный кабинет</li>
							<li class="active">номера телефонов</li>
							<li class="active">парсинг по расписанию</li>
							<li class="active">отправка файлов на почту</li>
						</ul>
					</div>
					<div class="pricing-footer">
						<a href="/internal/register/?t=3" class="main-button">Заказать</a>
					</div>
				</div>
			</div>
			<!-- ***** Pricing Item End ***** -->
		</div>
	</div>
</section>
<!-- ***** Pricing Plans End ***** -->

<!-- ***** Counter Parallax Start ***** -->
<section class="counter">
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6 col-sm-12">
					<div class="count-item decoration-bottom">
						<!--
						<strong>126</strong>
						<span>Projects</span>
						-->
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12">
					<div class="count-item decoration-top">
						<!--
						<strong>126</strong>
						<span>Projects</span>
						-->
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12">
					<div class="count-item decoration-bottom">
						<!--
						<strong>126</strong>
						<span>Projects</span>
						-->
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12">
					<div class="count-item">
						<!--
						<strong>126</strong>
						<span>Projects</span>
						-->
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- ***** Counter Parallax End ***** -->
<!-- ***** Contact Us Start ***** -->
<section class="section colored" id="contact-us">
	<div class="container">
		<!-- ***** Section Title Start ***** -->
		<div class="row">
			<div class="col-lg-12">
				<div class="center-heading">
					<h2 class="section-title">Свяжитесь с нами</h2>
				</div>
			</div>
			<div class="offset-lg-3 col-lg-6">
				<div class="center-text">
					<p>Вользуйтесь <a href="/internal/feedback/">формой</a> обратной связи по всем возникающим вопросам.</p>
				</div>
			</div>
		</div>
		<!-- ***** Section Title End ***** -->
	</div>
</section>
<!-- ***** Contact Us End ***** -->