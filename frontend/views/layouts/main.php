<?php

/* @var $this \yii\web\View */

/* @var $content string */

use frontend\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Парсинг и выгрузка обьявлений сайта Авито с телефонными номерами в JSON и CSV">
	<meta name="author" content="T-gate Soft">
	<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,700,900" rel="stylesheet">
	<?php $this->registerCsrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?> - T-Gate Soft</title>
	<?= $this->render('counters') ?>
	<?php $this->head() ?>
</head>
<body>

<div id="preloader">
	<div class="jumper">
		<div></div>
		<div></div>
		<div></div>
	</div>
</div>
<header class="header-area header-sticky">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<nav class="main-nav">
					<a href="#" class="logo">
						<img src="/images/logo.png" alt="T-Gate Soft"/>
					</a>
					<ul class="nav">
						<li><a href="#welcome" class="active">Начало</a></li>
						<li><a href="#features">О нас</a></li>
						<li><a href="#work-process">Примеры</a></li>
						<li><a href="#pricing-plans">Тарифы</a></li>
						<?php if (Yii::$app->user->isGuest): ?>
							<li><a href="/internal/register/">Регистрация</a></li>
						<?php endif ?>
						<li><a href="/internal/profile/"><?= (Yii::$app->user->isGuest ? 'Войти' : 'Личный кабинет') ?></a></li>
					</ul>
					<a class='menu-trigger'>
						<span>Меню</span>
					</a>
				</nav>
			</div>
		</div>
	</div>
</header>
<?php $this->beginBody() ?>
<?= $content ?>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">

			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<p class="copyright"> &copy; T-Gate Soft
					<script>
						document.write(new Date().getFullYear())
					</script>
				</p>
			</div>
		</div>
	</div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
