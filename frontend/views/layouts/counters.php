<!-- Yandex.Metrika counter -->
<script type="text/javascript">
	(function (m, e, t, r, i, k, a) {
		m[i] = m[i] || function () {
			(m[i].a = m[i].a || []).push(arguments)
		};
		m[i].l = 1 * new Date();
		k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
	})
	(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

	ym(61885927, "init", {
		clickmap: true,
		trackLinks: true,
		accurateTrackBounce: true
	});
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139683220-2"></script>
<script>
	window.dataLayer = window.dataLayer || [];

	function gtag() {
		dataLayer.push(arguments);
	}

	gtag('js', new Date());

	gtag('config', 'UA-139683220-2');
</script>

<meta name="yandex-verification" content="b2a3e6f500f8513b"/>
<meta name="google-site-verification" content="HJrTQx0qZhww3a8RUZ6KpM0QVRrG7tNXA8yq7V6EXaI"/>
<noscript>
	<div><img src="https://mc.yandex.ru/watch/61885927" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->