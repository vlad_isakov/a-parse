<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle {
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'css/bootstrap.min.css',
		'css/font-awesome.css',
		'css/templatemo-softy-pinko.css',
	];
	public $js = [
		'js/jquery-2.1.0.min.js',
		'js/popper.js',
		'js/bootstrap.min.js',
		'js/scrollreveal.min.js',
		'js/waypoints.min.js',
		'js/jquery.counterup.min.js',
		'js/imgfix.min.js',
		'js/custom.js',

	];
	/*public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
	];*/
}
