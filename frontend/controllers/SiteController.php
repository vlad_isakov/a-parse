<?php

namespace frontend\controllers;

use frontend\models\ContactForm;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller {
	/**
	 * Displays homepage.
	 *
	 * @return mixed
	 */
	public function actionIndex() {
		$this->view->title = 'Парсинг обьявлений Авито';

		return $this->render('index');
	}
}