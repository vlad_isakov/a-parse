<?php

namespace common\models;

use common\behaviors\WhoIsBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\validators\BooleanValidator;
use yii\validators\RequiredValidator;

/**
 * Поля таблицы:
 * @property integer $id
 * @property string  $url
 * @property boolean $active
 * @property integer $delay
 * @property string  $insert_stamp
 * @property string  $update_stamp
 * @property integer $insert_user
 * @property integer $update_user
 */
class Proxy extends ActiveRecord {

	const ATTR_ID = 'id';
	const ATTR_URL = 'url';
	const ATTR_ACTIVE = 'active';
	const ATTR_DELAY = 'delay';
	const ATTR_INSERT_STAMP = 'insert_stamp';
	const ATTR_UPDATE_STAMP = 'update_stamp';
	const ATTR_INSERT_USER = 'insert_user';
	const ATTR_UPDATE_USER = 'update_user';

	/**
	 * @inheritDoc
	 *
	 * @author Исаков Владислав
	 */
	public function behaviors() {
		return [
			[
				'class'              => TimestampBehavior::class,
				'createdAtAttribute' => static::ATTR_INSERT_STAMP,
				'updatedAtAttribute' => static::ATTR_UPDATE_STAMP,
				'value'              => new Expression('current_timestamp'),
			],
			[
				'class'                        => WhoIsBehavior::class,
				WhoIsBehavior::ATTR_WHO_CREATE => static::ATTR_INSERT_USER,
				WhoIsBehavior::ATTR_WHO_UPDATE => static::ATTR_UPDATE_USER,
				WhoIsBehavior::ATTR_VALUE      => Yii::$app->user->id,
			],
		];
	}

	public static function tableName() {
		return '{{%proxy}}';
	}

	/**
	 * @return array
	 *
	 * @author Исаков Владислав
	 */
	public function attributeLabels() {
		return [
			static::ATTR_ID           => 'id',
			static::ATTR_URL          => 'Адрес',
			static::ATTR_ACTIVE       => 'Активен',
			static::ATTR_DELAY        => 'Задержка',
			static::ATTR_INSERT_STAMP => 'insert_stamp',
			static::ATTR_UPDATE_STAMP => 'update_stamp',
			static::ATTR_INSERT_USER  => 'insert_user',
			static::ATTR_UPDATE_USER  => 'update_user',
		];
	}

	/**
	 * @return array
	 *
	 * @author Исаков Владислав
	 */
	public function rules() {
		return [
			[static::ATTR_URL, RequiredValidator::class],
			[static::ATTR_ACTIVE, BooleanValidator::class]
		];
	}
}
