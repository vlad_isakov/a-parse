<?php

namespace common\models;

use common\base\helpers\CurlHelper;
use common\base\helpers\ProxyHelper;
use common\behaviors\WhoIsBehavior;
use common\components\entities\AvitoItem;
use DiDom\Document;
use Exception;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\validators\BooleanValidator;
use yii\validators\DateValidator;
use yii\validators\NumberValidator;
use yii\validators\RequiredValidator;
use yii\validators\UrlValidator;

/**
 * Поля таблицы:
 * @property integer                          $id
 * @property string                           $url
 * @property integer                          $status
 * @property integer                          $count
 * @property integer                          $completed
 * @property bool                             $is_repeat
 * @property bool                             $is_send_to_email
 * @property string                           $repeat_stamp
 * @property string                           $insert_stamp
 * @property string                           $update_stamp
 * @property integer                          $insert_user
 * @property integer                          $update_user
 * @property integer                          $source_id
 *
 * @property-read \common\models\ObjectFile[] $files
 *
 */
class Task extends ActiveRecord {

	const ATTR_ID = 'id';
	const ATTR_URL = 'url';
	const ATTR_STATUS = 'status';
	const ATTR_COUNT = 'count';
	const ATTR_COMPLETED = 'completed';
	const ATTR_IS_REPEAT = 'is_repeat';
	const ATTR_REPEAT_STAMP = 'repeat_stamp';
	const ATTR_IS_SEND_TO_EMAIL = 'is_send_to_email';
	const ATTR_INSERT_STAMP = 'insert_stamp';
	const ATTR_UPDATE_STAMP = 'update_stamp';
	const ATTR_INSERT_USER = 'insert_user';
	const ATTR_UPDATE_USER = 'update_user';
	const ATTR_SOURCE_ID = 'source_id';

	const STATUS_IN_QUEUE = 0;
	const STATUS_IN_PROGRESS = 1;
	const STATUS_COMPLETE = 2;
	const STATUS_ERROR = 3;
	const STATUS_LIMIT_EXCEED = 4;

	const STATUS_NAMES = [
		self::STATUS_IN_QUEUE     => 'В очереди',
		self::STATUS_IN_PROGRESS  => 'Выполняется',
		self::STATUS_COMPLETE     => 'Завершено',
		self::STATUS_ERROR        => 'Ошибка',
		self::STATUS_LIMIT_EXCEED => 'Достигнут ежедневный лимит',
	];

	/**
	 * @inheritDoc
	 *
	 * @author Исаков Владислав
	 */
	public function behaviors() {
		return [
			[
				'class'              => TimestampBehavior::class,
				'createdAtAttribute' => static::ATTR_INSERT_STAMP,
				'updatedAtAttribute' => static::ATTR_UPDATE_STAMP,
				'value'              => new Expression('current_timestamp'),
			],
			[
				'class'                        => WhoIsBehavior::class,
				WhoIsBehavior::ATTR_WHO_CREATE => static::ATTR_INSERT_USER,
				WhoIsBehavior::ATTR_WHO_UPDATE => static::ATTR_UPDATE_USER,
				WhoIsBehavior::ATTR_VALUE      => Yii::$app->user->id,
			],
		];
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав
	 */
	public static function tableName() {
		return '{{%task}}';
	}

	/**
	 * @return array
	 *
	 * @author Исаков Владислав
	 */
	public function attributeLabels() {
		return [
			static::ATTR_ID               => 'id',
			static::ATTR_URL              => 'URL',
			static::ATTR_STATUS           => 'Статус',
			static::ATTR_COUNT            => 'Кол-во записей',
			static::ATTR_COMPLETED        => '%',
			static::ATTR_IS_REPEAT        => 'Повторять',
			static::ATTR_IS_SEND_TO_EMAIL => 'Отправлять на почту',
			static::ATTR_REPEAT_STAMP     => 'Время автоматической выгрузки',
			static::ATTR_INSERT_STAMP     => 'Добавлено',
			static::ATTR_UPDATE_STAMP     => 'Обновлено',
			static::ATTR_INSERT_USER      => 'Добавил',
			static::ATTR_UPDATE_USER      => 'Обновил',
		];
	}

	/**
	 * @return array
	 *
	 * @author Исаков Владислав
	 */
	public function rules() {
		return [
			[static::ATTR_COUNT, NumberValidator::class, 'integerOnly' => true],
			[static::ATTR_COUNT, RequiredValidator::class],
			[static::ATTR_URL, RequiredValidator::class],
			[static::ATTR_URL, UrlValidator::class],
			[static::ATTR_URL, 'siteValidator'],
			[static::ATTR_IS_REPEAT, BooleanValidator::class],
			[static::ATTR_IS_SEND_TO_EMAIL, BooleanValidator::class],
			[static::ATTR_REPEAT_STAMP, DateValidator::class],
		];
	}

	/**
	 * @param $attribute
	 * @param $params
	 *
	 * @author Исаков Владислав
	 */
	public function siteValidator($attribute, $params) {
		if (false === strpos($this->url, 'https://www.avito.ru')) {
			$this->addError($attribute, 'Незвестный сайт');
		}
	}

	/**
	 *
	 *
	 * @throws \DiDom\Exceptions\InvalidSelectorException
	 * @throws \yii\db\Exception
	 *
	 * @author Исаков Владислав
	 */
	public function processAvito() {

		$actualLimitCount = UserLimit::checkTodayLimit($this->insert_user, Tariff::SOURCE_AVITO);

		if (false === $actualLimitCount) {
			$this->status = static::STATUS_LIMIT_EXCEED;
			$this->save();

			return;
		}

		$parseToJsonResult = [];
		$parseToCsvResult = [];

		for ($page = 0; $page < $this->count / 50; $page++) {
			if ($page > 0) {
				$this->url .= '?p=' . $page;
			}

			$curl = curl_init($this->url);
			$options = CurlHelper::getRandomOptions('https://www.avito.ru', true);
			curl_setopt_array($curl, $options);

			$result = curl_exec($curl);

			$info = curl_getinfo($curl);

			if ($info['url'] === 'https://www.avito.ru/blocked') {
				$this->status = static::STATUS_IN_QUEUE;
				$this->save();

				curl_close($curl);
				ProxyHelper::blockProxy($options, true);
				die;
			}

			if (false === $result || '' === $result) {
				print_r('no list: ' . $result);
				flush();
				ProxyHelper::blockProxy($options, false);

				$this->processAvito();

				return;
			}

			curl_close($curl);

			$html = new Document();
			$html->loadHtml($result);

			$urls = [];
			$links = $html->find('.snippet-link');

			foreach ($links as $link) {
				$urls[] = 'https://www.avito.ru' . $link->getAttribute('href');
			}

			shuffle($urls); //перемешаем на всякий случай, чтобы не идти как робот по очереди
			foreach ($urls as $url) {
				//Проверим не упираемся ли в лимит
				if ($this->completed >= $actualLimitCount) {
					$this->saveDataToFiles($parseToJsonResult, $parseToCsvResult, true);

					return;
				}

				usleep(rand(1000000, 1500000));
				$avitoItem = new AvitoItem();
				$avitoItem->categoryUrl = $this->url;
				try {
					$avitoItem->getDataFromDom($url, $this->url);
				}
				catch (Exception $e) {
					usleep(10000);
					continue;
				}

				$parseToJsonResult[] = $avitoItem;
				//сконвертируем массивы для выгрузки в CSV
				$avitoItemCsv = clone $avitoItem;
				$csvParams = [];
				foreach ($avitoItemCsv->params as $param => $value) {
					$csvParams[] = $param . ': ' . $value;
				}

				$avitoItemCsv->images = implode(PHP_EOL, $avitoItemCsv->images);
				$avitoItemCsv->params = implode(PHP_EOL, $csvParams);
				unset($avitoItemCsv->categoryUrl);
				$parseToCsvResult[] = $avitoItemCsv;

				$this->completed++;

				if ($this->completed === $this->count) {
					break;
				}

				$this->save();
			}
		}


		$this->saveDataToFiles($parseToJsonResult, $parseToCsvResult);
	}

	/**
	 * Сохранение результатов парсинга в файлы
	 *
	 * @param array $parseToJsonResult
	 * @param array $parseToCsvResult
	 * @param bool  $limitExceed
	 *
	 * @author Исаков Владислав
	 */
	public function saveDataToFiles($parseToJsonResult, $parseToCsvResult, $limitExceed = false) {
		$fileHash = md5($this->url . time());

		$fp = fopen(Yii::getAlias('@files') . DIRECTORY_SEPARATOR . $fileHash . '.csv', 'w');
		fputcsv($fp, ['ID', 'Название', 'Цена', 'Характеристики', 'Телефон', 'Ссылка', 'Описание', 'Изображения', 'Адрес']);
		foreach ($parseToCsvResult as $fields) {
			fputcsv($fp, get_object_vars($fields));
		}
		fclose($fp);
		chmod(Yii::getAlias('@files') . DIRECTORY_SEPARATOR . $fileHash . '.csv', 0775);

		$parseToJsonResult = json_encode($parseToJsonResult);

		file_put_contents(Yii::getAlias('@files') . DIRECTORY_SEPARATOR . $fileHash . '.json', $parseToJsonResult);
		chmod(Yii::getAlias('@files') . DIRECTORY_SEPARATOR . $fileHash . '.json', 0775);

		$objectFile = new ObjectFile();
		$objectFile->object = static::class;
		$objectFile->object_id = $this->id;
		$objectFile->filename = $fileHash . '.csv';
		$objectFile->save();

		$objectFile = new ObjectFile();
		$objectFile->object = static::class;
		$objectFile->object_id = $this->id;
		$objectFile->filename = $fileHash . '.json';
		$objectFile->save();

		if ($this->count === $this->completed) {
			$this->status = static::STATUS_COMPLETE;
		}
		else {
			$this->status = static::STATUS_ERROR;

			if ($limitExceed) {
				$this->status = static::STATUS_LIMIT_EXCEED;
			}
		}

		ParseStatistic::add($this->insert_user, $this->url, Tariff::SOURCE_AVITO, $this->completed);
		$this->updateLimit();
		$this->save();
	}

	/**
	 * @return array|ObjectFile[]
	 *
	 * @author Исаков Владислав
	 */
	public function getFiles() {
		return ObjectFile::find()
			->andWhere([ObjectFile::ATTR_OBJECT => static::class])
			->andWhere([ObjectFile::ATTR_OBJECT_ID => $this->id])
			->all();
	}


	/**
	 * Обновление лимита
	 *
	 * @author Исаков Владислав
	 */
	public function updateLimit() {
		$limit = UserLimit::find()
			->andWhere([UserLimit::ATTR_USER_ID => $this->insert_user])
			->andWhere([UserLimit::ATTR_SOURCE_ID => Tariff::SOURCE_AVITO])
			->andWhere([UserLimit::ATTR_DATE_STAMP => new Expression('current_date')])
			->one();

		if (null === $limit) {
			$limit = new UserLimit();
			$limit->count = 0;
		}

		$limit->user_id = $this->insert_user;
		$limit->source_id = Tariff::SOURCE_AVITO;
		$limit->date_stamp = new Expression('current_date');
		$limit->count = $limit->count + $this->completed;
		$limit->save();
	}
}
