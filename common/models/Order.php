<?php

namespace common\models;

use common\behaviors\WhoIsBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Поля таблицы:
 * @property integer                    $id
 * @property string                     $number
 * @property integer                    $status
 * @property integer                    $user_id
 * @property integer                    $tariff_id
 * @property string                     $payment_stamp
 * @property string                     $insert_stamp
 * @property string                     $update_stamp
 * @property integer                    $insert_user
 * @property integer                    $update_user
 * @property string                     $kassa_id
 *
 * @property-read \common\models\Tariff $tariff
 * @property-read \common\models\User   $user
 *
 */
class Order extends ActiveRecord {

	const ATTR_ID = 'id';
	const ATTR_NUMBER = 'number';
	const ATTR_STATUS = 'status';
	const ATTR_USER_ID = 'user_id';
	const ATTR_TARIFF_ID = 'tariff_id';
	const ATTR_PAYMENT_STAMP = 'payment_stamp';
	const ATTR_INSERT_STAMP = 'insert_stamp';
	const ATTR_UPDATE_STAMP = 'update_stamp';
	const ATTR_INSERT_USER = 'insert_user';
	const ATTR_UPDATE_USER = 'update_user';
	const ATTR_KASSA_ID = 'kassa_id';

	const STATUS_NOT_PAYED = 0;
	const STATUS_PAYED = 1;

	const STATUS_NAMES = [
		self::STATUS_NOT_PAYED => 'Ожидание оплаты',
		self::STATUS_PAYED     => 'Оплачен',
	];

	/**
	 * @inheritDoc
	 *
	 * @author Исаков Владислав
	 */
	public function behaviors() {
		return [
			[
				'class'              => TimestampBehavior::class,
				'createdAtAttribute' => static::ATTR_INSERT_STAMP,
				'updatedAtAttribute' => static::ATTR_UPDATE_STAMP,
				'value'              => new Expression('current_timestamp'),
			],
			[
				'class'                        => WhoIsBehavior::class,
				WhoIsBehavior::ATTR_WHO_CREATE => static::ATTR_INSERT_USER,
				WhoIsBehavior::ATTR_WHO_UPDATE => static::ATTR_UPDATE_USER,
				WhoIsBehavior::ATTR_VALUE      => Yii::$app->user->id,
			],
		];
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав
	 */
	public static function tableName() {
		return '{{%order}}';
	}

	/**
	 * @return array
	 *
	 * @author Исаков Владислав
	 */
	public function attributeLabels() {
		return [
			static::ATTR_ID            => 'id',
			static::ATTR_NUMBER        => 'number',
			static::ATTR_STATUS        => 'status',
			static::ATTR_USER_ID       => 'user_id',
			static::ATTR_TARIFF_ID     => 'tariff_id',
			static::ATTR_PAYMENT_STAMP => 'payment_stamp',
			static::ATTR_INSERT_STAMP  => 'insert_stamp',
			static::ATTR_UPDATE_STAMP  => 'update_stamp',
			static::ATTR_INSERT_USER   => 'insert_user',
			static::ATTR_UPDATE_USER   => 'update_user',
		];
	}

	/**
	 * @param \common\models\Tariff $tariff
	 *
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 *
	 * @author Исаков Владислав
	 */
	public function setNum($tariff) {
		$this->number = 'T-' . mb_strtoupper($tariff->title) . '-' . $this->id;
		$this->update();
	}

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав
	 */
	public function getTariff() {
		return $this->hasOne(Tariff::class, [Tariff::ATTR_ID => static::ATTR_TARIFF_ID]);
	}

	const REL_TARIFF = 'tariff';

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав
	 */
	public function getUser() {
		return $this->hasOne(User::class, [User::ATTR_ID => static::ATTR_USER_ID]);
	}

	const REL_USER = 'user';
}
