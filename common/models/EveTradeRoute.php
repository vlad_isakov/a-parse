<?php

namespace common\models;

use common\behaviors\WhoIsBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Поля таблицы:
 * @property integer $id
 * @property string  $name
 * @property integer $one_volume
 * @property string  $from
 * @property string  $to
 * @property integer $sell_price
 * @property integer $buy_price
 * @property string  $sell_security
 * @property string  $buy_security
 * @property integer $count
 * @property integer $profit
 * @property string  $insert_stamp
 * @property string  $update_stamp
 * @property integer $insert_user
 * @property integer $update_user
 */
class EveTradeRoute extends ActiveRecord {

	const ATTR_ID = 'id';
	const ATTR_NAME = 'name';
	const ATTR_ONE_VOLUME = 'one_volume';
	const ATTR_FROM = 'from';
	const ATTR_TO = 'to';
	const ATTR_SELL_PRICE = 'sell_price';
	const ATTR_BUY_PRICE = 'buy_price';
	const ATTR_SELL_SECURITY = 'sell_security';
	const ATTR_BUY_SECURITY = 'buy_security';
	const ATTR_COUNT = 'count';
	const ATTR_PROFIT = 'profit';
	const ATTR_INSERT_STAMP = 'insert_stamp';
	const ATTR_UPDATE_STAMP = 'update_stamp';
	const ATTR_INSERT_USER = 'insert_user';
	const ATTR_UPDATE_USER = 'update_user';

	const COUNT_ON_PAGE = 5000;

	/**
	 * @inheritDoc
	 *
	 * @author Исаков Владислав
	 */
	public function behaviors() {
		return [
			[
				'class'              => TimestampBehavior::class,
				'createdAtAttribute' => static::ATTR_INSERT_STAMP,
				'updatedAtAttribute' => static::ATTR_UPDATE_STAMP,
				'value'              => new Expression('current_timestamp'),
			],
			[
				'class'                        => WhoIsBehavior::class,
				WhoIsBehavior::ATTR_WHO_CREATE => static::ATTR_INSERT_USER,
				WhoIsBehavior::ATTR_WHO_UPDATE => static::ATTR_UPDATE_USER,
				WhoIsBehavior::ATTR_VALUE      => 0,
			],
		];
	}

	public static function tableName() {
		return '{{%eve_trade_route}}';
	}

	public function attributeLabels() {
		return [
			static::ATTR_ID            => 'id',
			static::ATTR_NAME          => 'name',
			static::ATTR_ONE_VOLUME    => 'one_volume',
			static::ATTR_FROM          => 'from',
			static::ATTR_TO            => 'to',
			static::ATTR_SELL_PRICE    => 'sell_price',
			static::ATTR_BUY_PRICE     => 'buy_price',
			static::ATTR_SELL_SECURITY => 'sell_security',
			static::ATTR_BUY_SECURITY  => 'buy_security',
			static::ATTR_COUNT         => 'count',
			static::ATTR_PROFIT        => 'profit',
			static::ATTR_INSERT_STAMP  => 'insert_stamp',
			static::ATTR_UPDATE_STAMP  => 'update_stamp',
			static::ATTR_INSERT_USER   => 'insert_user',
			static::ATTR_UPDATE_USER   => 'update_user',
		];
	}
}
