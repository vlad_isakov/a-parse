<?php

namespace common\models;

use common\behaviors\WhoIsBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Поля таблицы:
 * @property integer $id
 * @property string  $title
 * @property string  $slug
 * @property string  $description
 * @property integer $active
 * @property integer $cost
 * @property integer $count
 * @property string  $insert_stamp
 * @property string  $update_stamp
 * @property integer $insert_user
 * @property integer $update_user
 * @property boolean $may_is_phone
 * @property boolean $may_is_repeat
 * @property boolean $may_is_send_to_email
 * @property int     $source_id
 *
 */
class Tariff extends ActiveRecord {

	const ATTR_ID = 'id';
	const ATTR_TITLE = 'title';
	const ATTR_SLUG = 'slug';
	const ATTR_DESCRIPTION = 'description';
	const ATTR_ACTIVE = 'active';
	const ATTR_COST = 'cost';
	const ATTR_COUNT = 'count';
	const ATTR_INSERT_STAMP = 'insert_stamp';
	const ATTR_UPDATE_STAMP = 'update_stamp';
	const ATTR_INSERT_USER = 'insert_user';
	const ATTR_UPDATE_USER = 'update_user';
	const ATTR_MAY_IS_PHONE = 'may_is_phone';
	const ATTR_MAY_IS_REPEAT = 'may_is_repeat';
	const ATTR_MAY_IS_SEND_TO_EMAIL = 'may_is_send_to_email';
	const ATTR_SOURCE_ID = 'source_id';

	const TRIAL_ID = 4;

	const SOURCE_AVITO = 0;

	const SOURCE_NAMES = [
		self::SOURCE_AVITO => 'Avito'
	];

	/**
	 * @inheritDoc
	 *
	 * @author Исаков Владислав
	 */
	public function behaviors() {
		return [
			[
				'class'              => TimestampBehavior::class,
				'createdAtAttribute' => static::ATTR_INSERT_STAMP,
				'updatedAtAttribute' => static::ATTR_UPDATE_STAMP,
				'value'              => new Expression('current_timestamp'),
			],
			[
				'class'                        => WhoIsBehavior::class,
				WhoIsBehavior::ATTR_WHO_CREATE => static::ATTR_INSERT_USER,
				WhoIsBehavior::ATTR_WHO_UPDATE => static::ATTR_UPDATE_USER,
				WhoIsBehavior::ATTR_VALUE      => Yii::$app->user->id,
			],
		];
	}

	/**
	 * @inheritDoc
	 *
	 * @author Исаков Владислав
	 */
	public static function tableName() {
		return '{{%tariff}}';
	}

	/**
	 * @inheritDoc
	 *
	 * @author Исаков Владислав
	 */
	public function attributeLabels() {
		return [
			static::ATTR_ID                   => 'id',
			static::ATTR_TITLE                => 'title',
			static::ATTR_SLUG                 => 'slug',
			static::ATTR_DESCRIPTION          => 'description',
			static::ATTR_ACTIVE               => 'active',
			static::ATTR_COST                 => 'cost',
			static::ATTR_COUNT                => 'count',
			static::ATTR_INSERT_STAMP         => 'insert_stamp',
			static::ATTR_UPDATE_STAMP         => 'update_stamp',
			static::ATTR_INSERT_USER          => 'insert_user',
			static::ATTR_UPDATE_USER          => 'update_user',
			static::ATTR_MAY_IS_PHONE         => 'may_is_phone',
			static::ATTR_MAY_IS_REPEAT        => 'may_is_repeat',
			static::ATTR_MAY_IS_SEND_TO_EMAIL => 'may_is_send_to_email',
			static::ATTR_SOURCE_ID            => 'source_id',
		];
	}
}
