<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\caching\TagDependency;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;

/**
 * Модель пользователя
 *
 * @property integer                            $id
 * @property string                             $username
 * @property string                             $password_hash
 * @property string                             $password_reset_token
 * @property string                             $verification_token
 * @property string                             $email
 * @property string                             $auth_key
 * @property integer                            $status
 * @property integer                            $created_at
 * @property integer                            $updated_at
 * @property string                             $password write-only password
 *
 * @property-read \common\models\Notification[] $notification
 * @property-read \common\models\Tariff[]       $tariffs
 * @property-read \common\models\TariffUser[]   $tariffsDetails
 *
 */
class User extends ActiveRecord implements IdentityInterface {

	const ATTR_ID = 'id';
	const ATTR_USER_NAME = 'username';
	const ATTR_PASSWORD_HASH = 'password_hash';
	const ATTR_STATUS = 'status';

	const STATUS_DELETED = 0;
	const STATUS_INACTIVE = 9;
	const STATUS_ACTIVE = 10;


	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%user}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			['status', 'default', 'value' => self::STATUS_INACTIVE],
			['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public static function findIdentity($id) {
		return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
	}

	/**
	 * {@inheritdoc}
	 */
	public static function findIdentityByAccessToken($token, $type = null) {
		throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
	}

	/**
	 * Finds user by username
	 *
	 * @param string $username
	 *
	 * @return static|null
	 */
	public static function findByUsername($username) {
		return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
	}

	/**
	 * Finds user by password reset token
	 *
	 * @param string $token password reset token
	 *
	 * @return static|null
	 */
	public static function findByPasswordResetToken($token) {
		if (!static::isPasswordResetTokenValid($token)) {
			return null;
		}

		return static::findOne([
			'password_reset_token' => $token,
			'status'               => self::STATUS_ACTIVE,
		]);
	}

	/**
	 * Finds user by verification email token
	 *
	 * @param string $token verify email token
	 *
	 * @return static|null
	 */
	public static function findByVerificationToken($token) {
		return static::findOne([
			'verification_token' => $token,
			'status'             => self::STATUS_INACTIVE
		]);
	}

	/**
	 * Finds out if password reset token is valid
	 *
	 * @param string $token password reset token
	 *
	 * @return bool
	 */
	public static function isPasswordResetTokenValid($token) {
		if (empty($token)) {
			return false;
		}

		$timestamp = (int)substr($token, strrpos($token, '_') + 1);
		$expire = Yii::$app->params['user.passwordResetTokenExpire'];

		return $timestamp + $expire >= time();
	}

	/**
	 * {@inheritdoc}
	 */
	public function getId() {
		return $this->getPrimaryKey();
	}

	/**
	 * {@inheritdoc}
	 */
	public function getAuthKey() {
		return $this->auth_key;
	}

	/**
	 * {@inheritdoc}
	 */
	public function validateAuthKey($authKey) {
		return $this->getAuthKey() === $authKey;
	}

	/**
	 * Validates password
	 *
	 * @param string $password password to validate
	 *
	 * @return bool if password provided is valid for current user
	 */
	public function validatePassword($password) {
		return Yii::$app->security->validatePassword($password, $this->password_hash);
	}

	/**
	 * Generates password hash from password and sets it to the model
	 *
	 * @param string $password
	 */
	public function setPassword($password) {
		$this->password_hash = Yii::$app->security->generatePasswordHash($password);
	}

	/**
	 * Generates "remember me" authentication key
	 */
	public function generateAuthKey() {
		$this->auth_key = Yii::$app->security->generateRandomString();
	}

	/**
	 * Generates new password reset token
	 */
	public function generatePasswordResetToken() {
		$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
	}

	/**
	 * Generates new token for email verification
	 */
	public function generateEmailVerificationToken() {
		$this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
	}

	/**
	 * Removes password reset token
	 */
	public function removePasswordResetToken() {
		$this->password_reset_token = null;
	}

	/**
	 * Получение лимита по тарифу
	 *
	 * @param int $sourceId
	 *
	 * @return int
	 *
	 * @author Исаков Владислав
	 */
	public function getLimitByActiveTariff($sourceId) {
		$cacheKey = Yii::$app->cache->buildKey([static::class, $this->id, $sourceId]);
		$activeTariff = Yii::$app->cache->get($cacheKey);

		if (false === $activeTariff) {
			/** @var \common\models\TariffUser $activeTariff */
			$activeTariff = TariffUser::find()
				->joinWith(TariffUser::REL_TARIFF)
				->andWhere([TariffUser::ATTR_USER_ID => $this->id])
				->andWhere([Tariff::tableName() . '.' . Tariff::ATTR_SOURCE_ID => $sourceId])
				->one();

			Yii::$app->cache->set($cacheKey, $activeTariff, 3600 * 60 * 24, new TagDependency(['tags' => TariffUser::class]));
		}

		//Если тарифа нет то возвращаем 0
		if (null === $activeTariff) {
			return 0;
		}

		return $activeTariff->tariff->count;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав
	 */
	public function getNotification() {
		return $this->hasMany(Notification::class, [Notification::ATTR_USER_ID => static::ATTR_ID]);
	}

	const REL_NOTIFICATION = 'notification';

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @throws \yii\base\InvalidConfigException
	 *
	 * @author Исаков Владислав
	 */
	public function getTariffs() {
		return $this->hasMany(Tariff::class, [Tariff::ATTR_ID => TariffUser::ATTR_TARIFF_ID])
			->viaTable(TariffUser::tableName(), [TariffUser::ATTR_USER_ID => static::ATTR_ID]);
	}

	const REL_TARIFFS = 'tariffs';

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @throws \yii\base\InvalidConfigException
	 *
	 * @author Исаков Владислав
	 */
	public function getTariffsDetails() {
		return $this->hasMany(TariffUser::class, [TariffUser::ATTR_USER_ID => static::ATTR_ID]);
	}

	const REL_TARIFFS_DETAILS = 'tariffsDetails';

	/**
	 *
	 * @param int $sourceId
	 *
	 * @return int
	 *
	 * @author Исаков Владислав
	 */
	public function getTodayLimit($sourceId) {
		/** @var UserLimit $limit */
		$limit = UserLimit::find()->andWhere([UserLimit::ATTR_USER_ID => $this->id])
			->andWhere([UserLimit::ATTR_DATE_STAMP => new Expression('current_date')])
			->andWhere([UserLimit::ATTR_SOURCE_ID => $sourceId])
			->one();

		if (null === $limit) {
			return 0;
		}

		return $limit->count;
	}
}
