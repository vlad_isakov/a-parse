<?php

namespace common\models;

use common\behaviors\WhoIsBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\validators\EachValidator;
use yii\validators\EmailValidator;
use yii\validators\FileValidator;
use yii\validators\NumberValidator;
use yii\validators\RequiredValidator;
use yii\validators\StringValidator;
use yii\validators\UrlValidator;

/**
 * Поля таблицы:
 * @property integer $id
 * @property string  $title
 * @property string  $slug
 * @property integer $city_id
 * @property integer $nearest_location_id
 * @property double  $lat
 * @property double  $lon
 * @property string  $description
 * @property string  $tags
 * @property string  $phones
 * @property string  $emails
 * @property string  $twitter
 * @property string  $instagram
 * @property string  $web_site
 * @property string  $address
 * @property string  $cover_image
 * @property integer $rating
 * @property integer $type
 * @property string  $insert_stamp
 * @property string  $update_stamp
 * @property integer $insert_user
 * @property integer $update_user
 */
class Place extends ActiveRecord {

	/** @var \yii\web\UploadedFile */
	public $coverFile;
	const ATTR_COVER_FILE = 'coverFile';

	const ATTR_ID = 'id';
	const ATTR_TITLE = 'title';
	const ATTR_SLUG = 'slug';
	const ATTR_CITY_ID = 'city_id';
	const ATTR_NEAREST_LOCATION_ID = 'nearest_location_id';
	const ATTR_LAT = 'lat';
	const ATTR_LON = 'lon';
	const ATTR_DESCRIPTION = 'description';
	const ATTR_TAGS = 'tags';
	const ATTR_PHONES = 'phones';
	const ATTR_EMAILS = 'emails';
	const ATTR_TWITTER = 'twitter';
	const ATTR_INSTAGRAM = 'instagram';
	const ATTR_WEB_SITE = 'web_site';
	const ATTR_ADDRESS = 'address';
	const ATTR_COVER_IMAGE = 'cover_image';
	const ATTR_RATING = 'rating';
	const ATTR_TYPE = 'type';
	const ATTR_INSERT_STAMP = 'insert_stamp';
	const ATTR_UPDATE_STAMP = 'update_stamp';
	const ATTR_INSERT_USER = 'insert_user';
	const ATTR_UPDATE_USER = 'update_user';

	const TYPE_TOURBASE = 0;

	const TYPE_NAMES = [
		self::TYPE_TOURBASE => 'База отдыха'
	];

	/**
	 * @inheritDoc
	 *
	 * @author Исаков Владислав
	 */
	public function behaviors() {
		return [
			[
				'class'              => TimestampBehavior::class,
				'createdAtAttribute' => static::ATTR_INSERT_STAMP,
				'updatedAtAttribute' => static::ATTR_UPDATE_STAMP,
				'value'              => new Expression('current_timestamp'),
			],
			[
				'class'                        => WhoIsBehavior::class,
				WhoIsBehavior::ATTR_WHO_CREATE => static::ATTR_INSERT_USER,
				WhoIsBehavior::ATTR_WHO_UPDATE => static::ATTR_UPDATE_USER,
				WhoIsBehavior::ATTR_VALUE      => Yii::$app->user->id,
			],
			[
				'class'         => SluggableBehavior::class,
				'attribute'     => static::ATTR_TITLE,
				'slugAttribute' => static::ATTR_SLUG,
				'attributes'    => [
					ActiveRecord::EVENT_BEFORE_INSERT => static::ATTR_SLUG,
				],
			]
		];
	}

	/**
	 * @inheritDoc
	 *
	 * @author Исаков Владислав
	 */
	public static function tableName() {
		return '{{%place}}';
	}

	/**
	 * @inheritDoc
	 *
	 * @author Исаков Владислав
	 */
	public function attributeLabels() {
		return [
			static::ATTR_ID                  => 'id',
			static::ATTR_TITLE               => 'Название',
			static::ATTR_CITY_ID             => 'Основной город',
			static::ATTR_NEAREST_LOCATION_ID => 'Ближайший населенный пункт',
			static::ATTR_LAT                 => 'Широта',
			static::ATTR_LON                 => 'Долгота',
			static::ATTR_DESCRIPTION         => 'Описание',
			static::ATTR_TAGS                => 'Поисковые ключи',
			static::ATTR_PHONES              => 'Телефоны',
			static::ATTR_EMAILS              => 'Электронная почта',
			static::ATTR_TWITTER             => 'Twitter',
			static::ATTR_INSTAGRAM           => 'Instagram',
			static::ATTR_WEB_SITE            => 'Сайт',
			static::ATTR_ADDRESS             => 'Адрес',
			static::ATTR_COVER_FILE          => 'Главное изображение',
			static::ATTR_RATING              => 'Рэйтинг',
			static::ATTR_COVER_IMAGE         => 'Обложка',
			static::ATTR_TYPE                => 'Тип',
			static::ATTR_INSERT_STAMP        => 'Добавлено',
			static::ATTR_UPDATE_STAMP        => 'Обновлено',
			static::ATTR_INSERT_USER         => 'Добавил',
			static::ATTR_UPDATE_USER         => 'Обновил'
		];
	}

	/**
	 * @inheritDoc
	 *
	 * @author Исаков Владислав
	 */
	public function rules() {
		return [
			[static::ATTR_TITLE, StringValidator::class],
			[static::ATTR_CITY_ID, NumberValidator::class, 'integerOnly' => true],
			[static::ATTR_NEAREST_LOCATION_ID, NumberValidator::class],
			[static::ATTR_LAT, NumberValidator::class],
			[static::ATTR_LON, NumberValidator::class],
			[static::ATTR_DESCRIPTION, StringValidator::class],
			[static::ATTR_TAGS, EachValidator::class, 'rule' => [StringValidator::class]],
			[static::ATTR_PHONES, EachValidator::class, 'rule' => [StringValidator::class]],
			[static::ATTR_EMAILS, EachValidator::class, 'rule' => [EmailValidator::class]],
			[static::ATTR_TWITTER, StringValidator::class],
			[static::ATTR_INSTAGRAM, StringValidator::class],
			[static::ATTR_WEB_SITE, UrlValidator::class],
			[static::ATTR_ADDRESS, StringValidator::class],
			[static::ATTR_COVER_FILE, FileValidator::class, 'mimeTypes' => 'image/png, image/jpg, image/jpeg', 'maxSize' => 20000],
			[static::ATTR_TYPE, NumberValidator::class, 'integerOnly' => true],
			[
				[
					static::ATTR_TITLE,
					static::ATTR_CITY_ID,
					static::ATTR_NEAREST_LOCATION_ID,
					static::ATTR_LAT,
					static::ATTR_LON,
					static::ATTR_DESCRIPTION,
					static::ATTR_TAGS,
					static::ATTR_TYPE,
				], RequiredValidator::class
			]
		];
	}

	/**
	 * Загрузка главного изображения
	 *
	 * @return bool
	 *
	 * @author Исаков Владислав
	 */
	public function uploadCover() {
		if ($this->validate()) {

			$fileName = md5($this->coverFile->baseName) . '.' . $this->coverFile->extension;
			$subDir = substr($fileName, 0, 2);
			$dir = '@placeCovers' . DIRECTORY_SEPARATOR . $subDir;

			if (!is_dir($dir)) {
				mkdir($dir, 755);
			}

			$this->cover_image = $subDir . DIRECTORY_SEPARATOR . $fileName;

			$this->coverFile->saveAs($dir . DIRECTORY_SEPARATOR . $fileName);

			return true;
		}
		else {
			return false;
		}
	}
}
