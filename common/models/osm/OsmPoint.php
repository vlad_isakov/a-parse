<?php

namespace common\models\osm;

use common\components\SiteModel;
use yii\db\ActiveRecord;

/**
 * Точки OpenStreetMap
 *
 * @property int    $osm_id
 * @property string $name
 * @property string $place
 *
 */
class OsmPoint extends ActiveRecord {
	const ATTR_ID = 'osm_id';
	const ATTR_NAME = 'name';
	const ATTR_PLACE = 'place';

	const PLACE_TYPES = [
		'village' => 'д.',
		'hamlet'  => 'д.',
		'town'    => 'г.',
		'city'    => 'г.',
	];

	public static function tableName() {
		return '{{%planet_osm_point}}';
	}
}