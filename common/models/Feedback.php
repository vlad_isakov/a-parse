<?php

namespace common\models;

use common\behaviors\WhoIsBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Поля таблицы:
 * @property integer $id
 * @property string  $name
 * @property integer $email
 * @property integer $text
 * @property integer $parent_id
 * @property string  $insert_stamp
 * @property string  $update_stamp
 * @property integer $insert_user
 * @property integer $update_user
 */
class Feedback extends ActiveRecord {

	const ATTR_ID = 'id';
	const ATTR_NAME = 'name';
	const ATTR_EMAIL = 'email';
	const ATTR_TEXT = 'text';
	const ATTR_PARENT_ID = 'parent_id';
	const ATTR_INSERT_STAMP = 'insert_stamp';
	const ATTR_UPDATE_STAMP = 'update_stamp';
	const ATTR_INSERT_USER = 'insert_user';
	const ATTR_UPDATE_USER = 'update_user';

	/**
	 * @inheritDoc
	 *
	 * @author Исаков Владислав
	 */
	public function behaviors() {
		return [
			[
				'class'              => TimestampBehavior::class,
				'createdAtAttribute' => static::ATTR_INSERT_STAMP,
				'updatedAtAttribute' => static::ATTR_UPDATE_STAMP,
				'value'              => new Expression('current_timestamp'),
			],
			[
				'class'                        => WhoIsBehavior::class,
				WhoIsBehavior::ATTR_WHO_CREATE => static::ATTR_INSERT_USER,
				WhoIsBehavior::ATTR_WHO_UPDATE => static::ATTR_UPDATE_USER,
				WhoIsBehavior::ATTR_VALUE      => Yii::$app->user->id,
			],
		];
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав
	 */
	public static function tableName() {
		return '{{%feedback}}';
	}

	/**
	 * @return array
	 *
	 * @author Исаков Владислав
	 */
	public function attributeLabels() {
		return [
			static::ATTR_ID           => 'id',
			static::ATTR_NAME         => 'name',
			static::ATTR_EMAIL        => 'email',
			static::ATTR_TEXT         => 'text',
			static::ATTR_PARENT_ID    => 'parent_id',
			static::ATTR_INSERT_STAMP => 'insert_stamp',
			static::ATTR_UPDATE_STAMP => 'update_stamp',
			static::ATTR_INSERT_USER  => 'insert_user',
			static::ATTR_UPDATE_USER  => 'update_user',
		];
	}
}
