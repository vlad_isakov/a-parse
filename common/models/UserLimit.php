<?php

namespace common\models;

use yii\db\ActiveRecord;
use yii\db\Exception;

/**
 * Поля таблицы:
 * @property integer $id
 * @property integer $user_id
 * @property string  $date_stamp
 * @property int     $source_id
 * @property integer $count
 */
class UserLimit extends ActiveRecord {

	const ATTR_ID = 'id';
	const ATTR_USER_ID = 'user_id';
	const ATTR_DATE_STAMP = 'date_stamp';
	const ATTR_SOURCE_ID = 'source_id';
	const ATTR_COUNT = 'count';

	public static function tableName() {
		return '{{%user_limit}}';
	}

	/**
	 * @return array
	 *
	 * @author Исаков Владислав
	 */
	public function attributeLabels() {
		return [
			static::ATTR_ID         => 'id',
			static::ATTR_USER_ID    => 'user_id',
			static::ATTR_DATE_STAMP => 'date_stamp',
			static::ATTR_COUNT      => 'count',
		];
	}

	/**
	 * Проверка сегодняшнего лимита
	 *
	 * @param int $userId
	 * @param int $sourceId
	 *
	 * @return bool|int
	 *
	 * @throws \yii\db\Exception
	 *
	 * @author Исаков Владислав
	 */
	public static function checkTodayLimit($userId, $sourceId) {
		/** @var \common\models\User $user */
		$user = User::findOne($userId);

		if (null === $user) {
			throw new Exception('Пользователь не найден');
		}

		$todayLimit = $user->getTodayLimit($sourceId);
		$tariffLimit = $user->getLimitByActiveTariff($sourceId);

		if ($tariffLimit > $todayLimit) {
			return $tariffLimit - $todayLimit;
		}

		return false;
	}
}
