<?php

namespace common\models;

use common\behaviors\WhoIsBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Поля таблицы:
 * @property integer                    $id
 * @property integer                    $user_id
 * @property integer                    $tariff_id
 * @property string                     $begin_stamp
 * @property string                     $end_stamp
 * @property string                     $insert_stamp
 * @property string                     $update_stamp
 * @property integer                    $insert_user
 * @property integer                    $update_user
 *
 * @property-read \common\models\Tariff $tariff
 */
class TariffUser extends ActiveRecord {

	const ATTR_ID = 'id';
	const ATTR_USER_ID = 'user_id';
	const ATTR_TARIFF_ID = 'tariff_id';
	const ATTR_BEGIN_STAMP = 'begin_stamp';
	const ATTR_END_STAMP = 'end_stamp';
	const ATTR_INSERT_STAMP = 'insert_stamp';
	const ATTR_UPDATE_STAMP = 'update_stamp';
	const ATTR_INSERT_USER = 'insert_user';
	const ATTR_UPDATE_USER = 'update_user';

	/**
	 * @inheritDoc
	 *
	 * @author Исаков Владислав
	 */
	public function behaviors() {
		return [
			[
				'class'              => TimestampBehavior::class,
				'createdAtAttribute' => static::ATTR_INSERT_STAMP,
				'updatedAtAttribute' => static::ATTR_UPDATE_STAMP,
				'value'              => new Expression('current_timestamp'),
			],
			[
				'class'                        => WhoIsBehavior::class,
				WhoIsBehavior::ATTR_WHO_CREATE => static::ATTR_INSERT_USER,
				WhoIsBehavior::ATTR_WHO_UPDATE => static::ATTR_UPDATE_USER,
				WhoIsBehavior::ATTR_VALUE      => Yii::$app->user->id,
			],
		];
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав
	 */
	public static function tableName() {
		return '{{%tariff_user}}';
	}

	/**
	 * @return array
	 *
	 * @author Исаков Владислав
	 */
	public function attributeLabels() {
		return [
			static::ATTR_ID           => 'id',
			static::ATTR_USER_ID      => 'user_id',
			static::ATTR_TARIFF_ID    => 'tariff_id',
			static::ATTR_BEGIN_STAMP  => 'begin_stamp',
			static::ATTR_END_STAMP    => 'end_stamp',
			static::ATTR_INSERT_STAMP => 'insert_stamp',
			static::ATTR_UPDATE_STAMP => 'update_stamp',
			static::ATTR_INSERT_USER  => 'insert_user',
			static::ATTR_UPDATE_USER  => 'update_user',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 *
	 * @author Исаков Владислав
	 */
	public function getTariff() {
		return $this->hasOne(Tariff::class, [Tariff::ATTR_ID => static::ATTR_TARIFF_ID]);
	}

	const REL_TARIFF = 'tariff';
}
