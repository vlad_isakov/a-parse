<?php

namespace common\models;

use common\behaviors\WhoIsBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Поля таблицы:
 * @property integer $id
 * @property integer $user_id
 * @property string  $url
 * @property integer $source_id
 * @property integer $count
 * @property string  $insert_stamp
 * @property string  $update_stamp
 * @property integer $insert_user
 * @property integer $update_user
 */
class ParseStatistic extends ActiveRecord {

	const ATTR_ID = 'id';
	const ATTR_USER_ID = 'user_id';
	const ATTR_URL = 'url';
	const ATTR_SOURCE_ID = 'source_id';
	const ATTR_COUNT = 'count';
	const ATTR_INSERT_STAMP = 'insert_stamp';
	const ATTR_UPDATE_STAMP = 'update_stamp';
	const ATTR_INSERT_USER = 'insert_user';
	const ATTR_UPDATE_USER = 'update_user';

	/**
	 * @inheritDoc
	 *
	 * @author Исаков Владислав
	 */
	public function behaviors() {
		return [
			[
				'class'              => TimestampBehavior::class,
				'createdAtAttribute' => static::ATTR_INSERT_STAMP,
				'updatedAtAttribute' => static::ATTR_UPDATE_STAMP,
				'value'              => new Expression('current_timestamp'),
			],
			[
				'class'                        => WhoIsBehavior::class,
				WhoIsBehavior::ATTR_WHO_CREATE => static::ATTR_INSERT_USER,
				WhoIsBehavior::ATTR_WHO_UPDATE => static::ATTR_UPDATE_USER,
				WhoIsBehavior::ATTR_VALUE      => Yii::$app->user->id,
			],
		];
	}

	public static function tableName() {
		return '{{%parse_statistic}}';
	}

	public function attributeLabels() {
		return [
			static::ATTR_ID           => 'id',
			static::ATTR_USER_ID      => 'user_id',
			static::ATTR_URL          => 'url',
			static::ATTR_SOURCE_ID    => 'source_id',
			static::ATTR_COUNT        => 'count',
			static::ATTR_INSERT_STAMP => 'insert_stamp',
			static::ATTR_UPDATE_STAMP => 'update_stamp',
			static::ATTR_INSERT_USER  => 'insert_user',
			static::ATTR_UPDATE_USER  => 'update_user',
		];
	}

	/**
	 * @param int    $userId
	 * @param string $url
	 * @param int    $sourceId
	 * @param int    $count
	 *
	 * @author Исаков Владислав
	 */
	public static function add($userId, $url, $sourceId, $count) {
		$record = new static();
		$record->user_id = $userId;
		$record->url = $url;
		$record->source_id = $sourceId;
		$record->count = $count;
		$record->save();
	}
}
