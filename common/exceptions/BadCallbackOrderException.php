<?php

namespace common\exceptions;

class BadCallbackOrderException extends OrderException {
	public $message = 'Неизвестный заказ при кэллбэке оплаты';
}