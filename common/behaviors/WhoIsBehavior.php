<?php

namespace common\behaviors;

use yii\behaviors\AttributeBehavior;
use yii\db\BaseActiveRecord;

/**
 * Установка и обновление полей кто создал и кто изменил запись
 *
 * @package common\behaviors
 *
 * @author  Исаков Владислав
 */
class WhoIsBehavior extends AttributeBehavior {
	/**
	 * @var string the attribute that will receive string value
	 * Set this property to false if you do not want to record the who is.
	 */
	public $whoCreateAttribute = 'insert_user';
	const ATTR_WHO_CREATE = 'whoCreateAttribute';

	/**
	 * @var string the attribute that will receive string value
	 * Set this property to false if you do not want to record the who is.
	 */
	public $whoUpdateAtAttribute = 'update_user';
	const ATTR_WHO_UPDATE = 'whoUpdateAtAttribute';

	/**
	 * @var string По-умолчанию Unknown
	 */
	public $value;
	const ATTR_VALUE = 'value';

	/**
	 * {@inheritdoc}
	 */
	public function init() {
		parent::init();

		if (empty($this->attributes)) {
			$this->attributes = [
				BaseActiveRecord::EVENT_BEFORE_INSERT => [$this->whoCreateAttribute, $this->whoUpdateAtAttribute],
				BaseActiveRecord::EVENT_BEFORE_UPDATE => $this->whoUpdateAtAttribute,
			];
		}
	}

	/**
	 * @param \yii\base\Event $event
	 *
	 * @return string
	 *
	 * @author Исаков Владислав
	 */
	protected function getValue($event) {
		if ($this->value === null) {
			return 'Unknown';
		}

		return parent::getValue($event);
	}
}
