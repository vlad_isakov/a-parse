<?php

namespace common\components\entities\avito;


class Error {
	const ERROR_UNKNOWN_KEY = 'Неизвестный ключ';

	/** @var string */
	public $code;

	/** @var string */
	public $message;
}