<?php

namespace common\components\entities\avito;

/**
 * Объект ответа сервера Авито о номере телефона
 *
 * @package common\components\entities\avito
 *
 * @author  Исаков Владислав
 */
class Response {
	const STATUS_BAD_REQUEST = 'bad-request';
	const STATUS_NOT_FOUND = 'not-found';
	const STATUS_OK = 'ok';

	/** @var \common\components\entities\avito\Error */
	public $error;

	/** @var string */
	public $status;

	/** @var \common\components\entities\avito\Result */
	public $result;

	/**
	 * @return bool
	 *
	 * @author Исаков Владислав
	 */
	public function hasError() {
		if (null === $this->error) {
			return false;
		}

		return true;
	}

	/**
	 * @return null|string
	 *
	 * @author Исаков Владислав
	 */
	public function getPhone() {
		if ($this->status === static::STATUS_BAD_REQUEST || $this->status === static::STATUS_NOT_FOUND) {
			return null;
		}

		return $this->result->action->extractPhone();
	}
}