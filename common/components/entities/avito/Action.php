<?php

namespace common\components\entities\avito;


class Action {
	/** @var string */
	public $title;

	/** @var string */
	public $uri;

	/**
	 * @return string
	 *
	 * @author Исаков Владислав
	 */
	public function extractPhone() {
		return str_replace('ru.avito://1/phone/call?number=%2B', '', $this->uri);
	}
}