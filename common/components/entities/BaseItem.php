<?php

namespace common\components\entities;

/**
 * Базовый класс обьекта парсинга
 *
 * @package common\components\entities
 *
 * @author  Исаков Владислав
 */
class BaseItem {

	const SOURCE_AVITO = 0;

	/** @var string|int */
	public $id;

	/** @var string */
	public $hash;

	/** @var string */
	public $title;

	/** @var float */
	public $price;

	/** @var array */
	public $params = [];

	/** @var string */
	public $phone;

	/** @var string */
	public $url;

	/** @var string */
	public $description;

	/** @var array */
	public $images = [];

	/** @var string */
	public $address;

	/** @var int */
	public $source;


}