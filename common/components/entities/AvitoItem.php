<?php

namespace common\components\entities;

use common\base\helpers\CurlHelper;
use common\base\helpers\ProxyHelper;
use common\components\entities\avito\Response;
use DiDom\Document;
use JsonMapper;

/**
 *
 * @package common\components\entities
 *
 * @author  Исаков Владислав
 */
class AvitoItem extends BaseItem {
	const PHONE_PARSE_URL_FORMAT = 'https://m.avito.ru/api/1/items/%d/phone?key=%s';

	/** @var string */
	public $categoryUrl;

	/** @var string */
	public $phoneParseKey = 'af0deccbgcgidddjgnvljitntccdduijhdinfgjgfjir';

	/**
	 * @author Isakov Vlad
	 *
	 * AvitoItem constructor.
	 */
	public function __construct() {
		$this->source = static::SOURCE_AVITO;
	}

	/**
	 * @param $url
	 * @param $categoryReferer
	 *
	 * @throws \DiDom\Exceptions\InvalidSelectorException
	 * @throws \JsonMapper_Exception
	 *
	 * @author Исаков Владислав
	 */
	public function getDataFromDom($url, $categoryReferer) {
		$this->url = $url;
		$this->_parseId();

		$curl = curl_init($this->url);
		$options = CurlHelper::getRandomOptions($categoryReferer, true);
		curl_setopt_array($curl, $options);
		$result = curl_exec($curl);

		$info = curl_getinfo($curl);

		if ($info['url'] === 'https://www.avito.ru/blocked' || $info['size_download'] == 0) {
			curl_close($curl);
			ProxyHelper::blockProxy($options);
		}

		if (false === $result || '' === $result) {
			echo 'no item';

			return;
		}

		curl_close($curl);

		$html = new Document();
		$html->loadHtml($result);

		$title = $html->find('[data-marker=item-description/title]');
		//desktop
		if ([] === $title) {
			$this->title = $html->find('.title-info-title-text')[0]->text();
			$this->price = (float)str_replace(' ', '', $html->find('[itemprop="price"]')[0]->text());
			$this->description = $html->find('[itemprop="description"]')[0]->text();
			$this->address = $html->find('.item-address__string')[0]->text();
			$images = $html->find('.gallery-img-frame');
			foreach ($images as $image) {
				$this->images[] = str_replace('//', 'https://', $image->getAttribute('data-url'));
			}
			$paramsList = $html->find('.item-params-list li');
			if ([] === $paramsList) {
				$params = explode(':', $html->find('.item-params')[0]->text());
				$this->params[trim($params[0])] = trim($params[1]);
			}
			else {
				foreach ($paramsList as $param) {
					$p = explode(':', $param->text());
					$this->params[trim($p[0])] = trim($p[1]);
				}
			}
		}
		//mobile
		else {
			$this->title = $title[0]->text();
			$this->price = (float)str_replace(' ', '', $html->find('[data-marker=item-description/price]')[0]->text());
			$this->description = $html->find('[data-marker=item-description/text]')[0]->text();
			$this->address = $html->find('[data-marker=delivery/location]')[0]->text();
			$images = $html->find('.lazy-load-image-loaded img');
			foreach ($images as $image) {
				$this->images[] = $image->getAttribute('src');
			}
		}

		$this->hash = md5($this->source . $this->id);

		$this->_searchPhone();
		$this->_clearData();
	}

	/**
	 * @throws \JsonMapper_Exception
	 *
	 * @author Исаков Владислав
	 */
	private function _searchPhone() {
		$phoneParseUrl = sprintf(static::PHONE_PARSE_URL_FORMAT, $this->id, $this->phoneParseKey);

		$curl = curl_init($phoneParseUrl);
		$referer = str_replace('www', 'm', $this->url);

		curl_setopt_array($curl, CurlHelper::getRandomOptions($referer));
		$result = curl_exec($curl);
		curl_close($curl);

		$mapper = new JsonMapper();
		$result = json_decode($result);
		/** @var Response $phoneResponse */
		$phoneResponse = $mapper->map(
			$result,
			new Response());

		if (!$phoneResponse->hasError()) {
			$this->phone = $phoneResponse->getPhone();
		}
	}

	/**
	 * @author Исаков Владислав
	 */
	private function _parseId() {
		$this->id = substr($this->url, strlen($this->url) - 10, 10);
	}

	/**
	 * @author Исаков Владислав
	 */
	private function _clearData() {
		$this->address = rtrim(ltrim(str_replace(PHP_EOL, '', $this->address)));
		$this->description = rtrim(ltrim(str_replace(PHP_EOL, '', $this->description)));
		unset($this->hash);
		unset($this->phoneParseKey);
		unset($this->source);
	}
}