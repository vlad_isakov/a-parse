<?php
/**
 * @author Исаков Владислав
 */

namespace common\components\validators;

use common\models\Place;
use yii\validators\Validator;

/**
 * Валидатор слов разделенных запятой
 *
 * @package common\components\validators
 *
 * @author  Исаков Владислав
 */
class CommaSeparatedValidator extends Validator {
	/** @var Validator */
	public $eachValidator;
	const ATTR_EACH_VALIDATOR = 'eachValidator';

	/**
	 * ёбаный стыд ну и костыль(( но селект2 не показывает значения((
	 *
	 * @param \yii\base\Model $model
	 * @param string          $attribute
	 *
	 * @author Исаков Владислав
	 */
	public function validateAttribute($model, $attribute) {
		$terms = explode(',', $model->$attribute);
		/** @var Validator $validator */
		$validator = new $this->eachValidator;

		foreach ($terms as $key => $term) {
			if (!$validator->validate(trim($term))) {
				$this->addError($model, $attribute, 'Неверное значение: ' . $term);

			}
			if ($attribute === Place::ATTR_PHONES && strlen(trim($term)) !== 11) {
				$this->addError($model, $attribute, 'Неверное значение телефона: ' . $term . '. Необходимо 11 символов. Например 89147186411');
			}
		}
	}
}