<?php

namespace common\components;

/**
 * Переопределение стандартного класса миграций
 *
 * @package common\components
 *
 * @author  Исаков Владислав
 */
class Migration extends \yii\db\Migration {

	/**
	 * Создание индекса
	 *
	 * @param null         $name
	 * @param string       $tableName
	 * @param array|string $field
	 * @param bool         $uq
	 *
	 * @author Исаков Владислав
	 */
	public function createIndex($name, $tableName, $field, $uq = false) {
		if (is_array($field)) {
			$alias = implode('-', $field);
		}
		else {
			$alias = $field;
		}

		if (null === $name) {
			if ($uq) {
				$name = 'uq-' . strtolower($tableName) . '-' . $alias;
			}
			else {
				$name = 'ix-' . strtolower($tableName) . '-' . $alias;
			}
		}

		$name = substr($name, 0, 30);

		parent::createIndex($name, $tableName, $field, $uq);
	}
}