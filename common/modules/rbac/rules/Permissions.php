<?php
namespace common\modules\rbac\rules;

class Permissions {
	//Разрешения
	const P_ADMIN = 'PermissionAdmin';

	//Роли
	const ROLE_ADMIN = 'Administrator';
	const ROLE_USER = 'User';
}