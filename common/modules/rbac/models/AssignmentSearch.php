<?php

namespace common\modules\rbac\models;

use common\models\User;
use common\modules\rbac\Module;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * @author John Martin <john.itvn@gmail.com>
 * @since  1.0.0
 *
 */
class AssignmentSearch extends Model {

	/**
	 * @var Module $rbacModule
	 */
	protected $rbacModule;

	/**
	 *
	 * @var mixed $id
	 */
	public $rowguid;

	/**
	 *
	 * @var string $User_Name
	 */
	public $User_Name;

	/**
	 * @inheritdoc
	 */
	public function init() {
		parent::init();
		$this->rbacModule = Yii::$app->getModule('rbac');
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['rowguid', 'User_Name'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'rowguid'   => Yii::t('rbac', 'ID'),
			'User_Name' => 'Пользователь',
		];
	}

	/**
	 * Create data provider for Assignment model.
	 */
	public function search() {
		$query = call_user_func($this->rbacModule->userModelClassName . "::find");
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		$params = Yii::$app->request->getQueryParams();

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([User::ATTR_GUID => $this->rowguid]);
		$query->andFilterWhere(['like', User::ATTR_USER_NAME, $this->User_Name]);

		return $dataProvider;
	}

}
