<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class'        => \yii\caching\MemCache::class,
            'useMemcached' => true,
            'servers'      => [
                [
                    'host'   => 'localhost',
                    'port'   => 11211,
                    'weight' => 60,
                ],
            ],
        ],
        'log'           => [
            'traceLevel' => 3,
            'targets'    => [
                [
                    'class'  => \common\components\LogDbTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
];
