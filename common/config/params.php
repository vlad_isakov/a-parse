<?php
return [
    'adminEmail' => 'help@t-gate.ru',
    'supportEmail' => 'help@t-gate.ru',
    'senderEmail' => 'help@t-gate.ru',
    'senderName' => 'T-gate soft',
    'user.passwordResetTokenExpire' => 3600,
    'user.passwordMinLength' => 8,
];
