<?php

namespace common\base\helpers;

use Yii;

class CurlHelper {

	const USER_AGENT = [
		'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:74.0) Gecko/20100101 Firefox/74.0',
		'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
		'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
		'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
		'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36',
		'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
		'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
		'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36',
		'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36',
		'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36',
		'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36',
		'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36',
		'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
		'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36',
		'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',
		'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
		'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
		'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36',
		'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
		'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
		'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36',
		'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'
	];

	/**
	 * @param string $referer
	 * @param bool   $useProxy
	 *
	 * @return array
	 *
	 * @author Исаков Владислав
	 */
	public static function getRandomOptions($referer, $useProxy = false) {
		$userAgent = static::USER_AGENT[rand(0, count(static::USER_AGENT) - 1)];
		$cookieFileName = 'common_cookie.txt';
		$proxyOptions = [];

		if ($useProxy) {
			$proxyList = ProxyHelper::getProxyList();
			if (count($proxyList) > 0) {
				$proxy = $proxyList[rand(0, count($proxyList) - 1)];

				//Привяжем файл куки к прокси-серверу и user agent'у, чтобы с разных IP шли запросы с привзанными к ним куками
				$cookieFileName = md5($proxy . $userAgent) . '.txt';
				$proxyOptions = [
					CURLOPT_PROXY     => $proxy,
					CURLOPT_PROXYTYPE => CURLPROXY_HTTP
				];
			}
			else {
				//кончились прокси, надо что-то делать
			}
		}

		$cookieFile = Yii::getAlias('@cookies') . DIRECTORY_SEPARATOR . $cookieFileName;

		$additionalHeaders = [
			'Referer: ' . $referer,
			'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
			'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
			'Dnt: 1',
			'Connection: keep-alive',
			'Cache-Control: max-age=0',
			'Upgrade-Insecure-Requests: 1'
		];

		return [
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_USERAGENT      => $userAgent,
				CURLOPT_REFERER        => $referer,
				CURLOPT_COOKIE         => true,
				CURLOPT_COOKIEJAR      => $cookieFile,
				CURLOPT_COOKIEFILE     => $cookieFile,
				CURLOPT_AUTOREFERER    => true,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_CONNECTTIMEOUT => 2,
				CURLOPT_ENCODING       => 'gzip, deflate',
				CURLOPT_MAXREDIRS      => 2,
				CURLOPT_HTTPHEADER     => $additionalHeaders
			] + $proxyOptions;
	}
}