<?php

namespace common\base\helpers;

use common\models\Proxy;
use Yii;
use yii\caching\TagDependency;

/**
 * Class ProxyHelper
 *
 * @package common\base\helpers
 *
 * @author  Исаков Владислав
 */
class ProxyHelper {

	/**
	 * @return \common\models\Proxy[]
	 *
	 * @author Исаков Владислав
	 */
	public static function getProxyList() {
		$cacheKey = Yii::$app->cache->buildKey(__METHOD__);
		$proxyList = Yii::$app->cache->get($cacheKey);

		if (false === $proxyList) {
			$proxyList = Proxy::findAll([Proxy::ATTR_ACTIVE => true]);

			Yii::$app->cache->set($cacheKey, $proxyList, null, new TagDependency(['tags' => Proxy::class]));
		}

		return $proxyList;
	}

	/**
	 * Проверка прокси-сервера
	 *
	 * @param string $proxy
	 *
	 * @return bool
	 *
	 * @author Исаков Владислав
	 */
	public static function checkProxy($proxy) {
		$curl = curl_init('https://www.avito.ru');

		$cookieFileName = md5($proxy) . '.txt';
		$cookieFile = Yii::getAlias('@cookies') . DIRECTORY_SEPARATOR . $cookieFileName;

		$options = [
			CURLOPT_USERAGENT      => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.1) Gecko/2008070208',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_COOKIESESSION  => true,
			CURLOPT_AUTOREFERER    => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_COOKIEJAR      => $cookieFile,
			CURLOPT_COOKIEFILE     => $cookieFile,
			CURLOPT_CONNECTTIMEOUT => 3,
			CURLOPT_PROXY          => $proxy,
			CURLOPT_PROXYTYPE      => CURLPROXY_HTTP
		];

		curl_setopt_array($curl, $options);
		$result = curl_exec($curl);
		$info = curl_getinfo($curl);

		$result = true;
		$blocked = false;
		//Проверим работает ли вообще этот прокси
		if ($info['size_download'] == 0) {
			$result = false;
		}

		//Не в блоке ли у Авито
		if ($info['url'] === 'https://www.avito.ru/blocked') {
			$result = false;
			$blocked = true;
		}

		//Если работает то проверим весь ли контент грузится
		if (false === strpos($result, '</html>')) {
			$result = false;
		}

		if (false === $result) {
			static::blockProxy($options, $blocked);
		}

		return $result;
	}

	/**
	 * Уберем из списка заблокированную прокси до выздоровления
	 *
	 * @param array $options
	 * @param bool  $blocked
	 *
	 * @author Исаков Владислав
	 */
	public static function blockProxy($options, $blocked = false) {
		if (!array_key_exists(CURLOPT_PROXY, $options)) {
			return;
		}

		$blockedProxy = $options[CURLOPT_PROXY];
		$proxy = Proxy::findOne([Proxy::ATTR_URL => $blockedProxy]);
		if (null !== $proxy) {
			$proxy->active = false;
			$proxy->save();
			TagDependency::invalidate(Yii::$app->cache, [Proxy::class]);
		}
	}
}