<?php

namespace common\base\helpers;

use Yii;

class ParseHelper {

	/**
	 * @author Исаков Владислав
	 */
	public static function saveRequest() {
		$data = json_encode($_SERVER);
		file_put_contents(Yii::getAlias('@dumps') . DIRECTORY_SEPARATOR . 'curl_dump.json', $data);
	}
}