<?php
/**
 * @author Исаков Владислав
 */

namespace common\base\helpers;

use Exception;

class PostgreSQLHelper {
	/**
	 * Encodes PHP array into PostgreSQL array data format
	 *
	 * @param array $value
	 *
	 * @return string PostgreSQL-encoded array
	 * @throws \Exception
	 */
	public static function arrayEncode($value) {
		if (empty($value) || !is_array($value)) {
			return null;
		}
		$result = '{';
		$firstElem = true;
		foreach ($value as $elem) {
			if (!$firstElem) {
				$result .= ',';
			}
			if (is_array($elem)) {
				$result .= static::arrayEncode($elem);
			}
			else if (is_string($elem)) {
				if (strpos($elem, ',') !== false) {
					$result .= '"' . $elem . '"';
				}
				else {
					$result .= $elem;
				}
			}
			else if (is_numeric($elem)) {
				$result .= $elem;
			}
			else {
				throw new Exception('Array contains other than string or numeric values, can\'t save to PostgreSQL array field');
			}
			$firstElem = false;
		}
		$result .= '}';

		return $result;
	}
}