<?php
[
	'menuItems' => [
		'catalog' => [
			'title' => 'Catalog',
			'icon' => 'fa fa-database',
			'url' => '#',
			'subItems' => [
				'subitem' => [
					'title' => 'Subcatalog',
					'icon' => 'fa fa-link',
					'url' => '/admin/catalog',
				]
			]
		],
		'otheritem' => [
			'title' => 'Other Item',
			'icon' => 'fa fa-database',
			'url' => '/admin/otheritem',
		],
	],
];