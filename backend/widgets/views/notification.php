<?php
/**
 * @author Исаков Владислав
 *
 * @var \common\models\Notification[] $notifications
 */

$count = count($notifications);
?>
<?php if ($count > 0): ?>
	<li class="nav-item dropdown">
		<a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<i class="material-icons">notifications</i>
			<span class="notification"><?= $count ?></span>
			<p class="d-lg-none d-md-block">
				Сообщения
			</p>
		</a>
		<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
			<?php foreach ($notifications as $notification): ?>
				<a class="dropdown-item" href="#"><?= $notification->type ?></a>
			<?php endforeach ?>
		</div>
	</li>
<?php endif ?>