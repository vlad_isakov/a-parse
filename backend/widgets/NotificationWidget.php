<?php

namespace backend\widgets;

use common\models\Notification;
use Yii;
use yii\base\Widget;

/**
 * Виджет сообщений
 *
 * @package backend\widgets
 *
 * @author  Исаков Владислав
 */
class NotificationWidget extends Widget {

	/**
	 * @return string
	 *
	 * @author Исаков Владислав
	 */
	public function run() {
		$notifications = Notification::find()
			->andWhere([Notification::ATTR_USER_ID => Yii::$app->user->id])
			->all();

		return $this->render('notification', ['notifications' => $notifications]);
	}
}