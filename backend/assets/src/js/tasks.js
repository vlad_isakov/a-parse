(function ($) {
	'use strict';
	$.fn.tasksPlugin = function () {
		const methods = {
			init: function () {
				let timerId = setTimeout(function tick() {
					$.pjax.reload({container: '#tasks', async: false});
					timerId = setTimeout(tick, 5000);
				}, 5000);
			}
		};
		methods.init();
	};
})(jQuery);