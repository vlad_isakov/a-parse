(function ($) {
	'use strict';
	$.fn.errorsPlugin = function () {
		const methods = {
			init: function () {
				let timerId = setTimeout(function tick() {
					$.pjax.reload({container: '#errors', async: false});
					timerId = setTimeout(tick, 10000);
				}, 10000);
			}
		};
		methods.init();
	};
})(jQuery);