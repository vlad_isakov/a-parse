(function ($) {
	'use strict';
	$.fn.tasksActionsPlugin = function () {
		const RENEW_URL = $('.renew-url').data('url');
		const methods = {
			init: function () {
				$('.renew-link').click(function () {
					$(this).hide();
					$.ajax({
						url: RENEW_URL + '?taskId=' + $(this).data('task-id'),
						method: "GET",
						async: true,
						beforeSend: function () {
						}
					}).done(function (data) {
					});
				});
			}
		};
		methods.init();
	};
})(jQuery);