(function ($) {
	'use strict';
	$.fn.proxyPlugin = function () {
		const methods = {
			init: function () {
				let timerId = setTimeout(function tick() {
					$.pjax.reload({container: '#proxy', async: false});
					timerId = setTimeout(tick, 10000);
				}, 10000);
			}
		};
		methods.init();
	};
})(jQuery);