<?php

namespace backend\models;

use common\models\Order;
use common\models\Tariff;
use common\models\TariffUser;
use common\models\User;
use himiklab\yii2\recaptcha\ReCaptchaValidator2;
use Yii;
use yii\base\Model;
use yii\db\Expression;

/**
 * @author Исаков Владислав
 */
class RegisterForm extends Model {
	public $username;
	public $password;
	public $email;
	public $reCaptcha;

	/**
	 * @return array
	 *
	 * @author Исаков Владислав
	 */
	public function rules() {
		return [
			['username', 'trim'],
			['username', 'required'],
			['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Это имя занято.'],
			['username', 'string', 'min' => 2, 'max' => 255],

			['email', 'trim'],
			['email', 'required'],
			['email', 'email'],
			['email', 'string', 'max' => 255],
			['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Этот почтовый адрес занят.'],

			['password', 'required'],
			['password', 'string', 'min' => 8],
			[
				['reCaptcha'], ReCaptchaValidator2::class,
				'uncheckedMessage' => 'Пожалуйста, подтвердите что вы не робот.'
			]
		];
	}

	public function attributeLabels() {
		return [
			'username' => 'Имя',
			'password' => 'Пароль',

		];
	}

	/**
	 * @return bool|null
	 *
	 * @parap  int $t
	 *
	 * @author Исаков Владислав
	 */
	public function signup($t = null) {
		if (!$this->validate()) {
			return null;
		}

		$user = new User();
		$user->username = strip_tags($this->username);
		$user->email = $this->email;
		$user->status = User::STATUS_ACTIVE; //Пока сразу активируем
		$user->setPassword($this->password);
		$user->generateAuthKey();
		$user->generateEmailVerificationToken();

		$result = $user->save();

		$trialTariff = new TariffUser();
		$trialTariff->insert_user = 0;
		$trialTariff->user_id = $user->id;
		$trialTariff->tariff_id = Tariff::TRIAL_ID;
		$trialTariff->begin_stamp = new Expression('current_timestamp');
		$trialTariff->end_stamp = new Expression('current_timestamp + interval \'31 day\'');
		$trialTariff->save();

		//Если пришли с выбранным тарифом то добавим его в заказ
		if ($result && null !== $t) {
			/** @var Tariff $tariff */
			$tariff = Tariff::find()->andWhere([Tariff::ATTR_ACTIVE => 1])
				->andWhere([Tariff::ATTR_ID => $t])
				->one();

			if (null !== $tariff) {
				$order = new Order();
				$order->user_id = $user->id;
				$order->tariff_id = $tariff->id;
				$order->status = Order::STATUS_NOT_PAYED;
				$order->save();
				$order->setNum($tariff);
			}
		}

		return $result;
	}

	/**
	 * @param $user
	 *
	 * @return bool
	 *
	 * @author Исаков Владислав
	 */
	protected function sendEmail($user) {
		return Yii::$app
			->mailer
			->compose(
				['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
				['user' => $user]
			)
			->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
			->setTo($this->email)
			->setSubject('Account registration at ' . Yii::$app->name)
			->send();
	}
}