<?php
/**
 * @author Исаков Владислав
 */

namespace backend\models;

use common\models\EveTradeRoute;
use phpDocumentor\Reflection\Types\Integer;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\Expression;
use yii\validators\BooleanValidator;
use yii\validators\NumberValidator;
use yii\validators\StringValidator;

class RouteSearchForm extends Model
{
    public $from;
    public $to;
    public $volumeFrom;
    public $volumeTo;
    public $profitFrom;
    public $profitTo;
    public $onlySafe = true;
    public $item;

    public $routes = [];
    public $pages;

    const ATTR_FROM = 'from';
    const ATTR_TO = 'to';
    const ATTR_VOLUME_FROM = 'volumeFrom';
    const ATTR_VOLUME_TO = 'volumeTo';
    const ATTR_PROFIT_FROM = 'profitFrom';
    const ATTR_PROFIT_TO = 'profitTo';
    const ATTR_ONLY_SAFE = 'onlySafe';
    const ATTR_ITEM_SAFE = 'item';

    public function rules() {
        return [
            [static::ATTR_FROM, StringValidator::class],
            [static::ATTR_TO, StringValidator::class],
            [static::ATTR_VOLUME_FROM, NumberValidator::class],
            [static::ATTR_VOLUME_TO, NumberValidator::class],
            [static::ATTR_PROFIT_FROM, NumberValidator::class],
            [static::ATTR_PROFIT_TO, NumberValidator::class],
            [static::ATTR_ONLY_SAFE, BooleanValidator::class],
        ];
    }

    public function search() {
        $query = EveTradeRoute::find();

        if ($this->onlySafe) {
            $query->andWhere(['>', EveTradeRoute::ATTR_SELL_SECURITY, '0.5'])
                ->andWhere(['>', EveTradeRoute::ATTR_BUY_SECURITY, '0.5']);
        }

        $countQuery = clone $query;
        $this->pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 50]);

        $this->routes = $query->offset($this->pages->offset)
            ->limit($this->pages->limit)
            ->orderBy([
                EveTradeRoute::ATTR_PROFIT => SORT_DESC,
                EveTradeRoute::ATTR_ONE_VOLUME => SORT_DESC ,
            ])
            ->all();
    }
}