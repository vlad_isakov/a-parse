<?php

namespace backend\models;

use common\models\Feedback;
use himiklab\yii2\recaptcha\ReCaptchaValidator2;
use Yii;
use yii\base\Model;
use yii\validators\StringValidator;

/**
 * ContactForm is the model behind the contact form.
 */
class FeedbackForm extends Model {
	public $name;
	public $email;
	public $subject;
	public $body;
	public $text;
	public $reCaptcha;

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['name', 'email', 'text'], 'required'],
			['text', StringValidator::class, 'max' => 250],
			['email', 'email'],
			[
				['reCaptcha'], ReCaptchaValidator2::class,
				'uncheckedMessage' => 'Пожалуйста, подтвердите что вы не робот.'
			]
		];
	}

	public function attributeLabels() {
		return [
			'name' => 'Имя',
			'text' => 'Сообщение',
		];
	}

	/**
	 * Sends an email to the specified email address using the information collected by this model.
	 *
	 * @param string $email the target email address
	 *
	 * @return bool whether the email was sent
	 */
	public function sendEmail($email) {
		return Yii::$app->mailer->compose()
			->setTo($email)
			->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
			->setReplyTo([$this->email => $this->name])
			->setSubject($this->subject)
			->setTextBody($this->body)
			->send();
	}

	/**
	 * @return bool
	 *
	 * @author Исаков Владислав
	 */
	public function saveMessage() {
		$message = new Feedback();
		$message->name = strip_tags($this->name);
		$message->text = strip_tags($this->text);
		$message->email = $this->email;

		Yii::$app->mailer->compose()
			->setFrom(Yii::$app->params['senderEmail'])
			->setTo(['help@t-gate.ru'])
			->setSubject('Сообщение из обратной связи')
			->setHtmlBody($message->name . '<br>' . $message->email . '<hr>' . $message->text)
			->send();

		return $message->save();
	}
}
