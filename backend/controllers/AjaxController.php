<?php
/**
 * @author Исаков Владислав
 */

namespace backend\controllers;

use Yii;
use yii\web\Response;

class AjaxController extends BackendController {

	const ACTION_GET_CITY = 'get-city';
	const ACTION_GET_AVITO_CATEGORY = 'get-avito-category';

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
		];
	}

	/**
	 * @inheritDoc
	 *
	 * @author Исаков Владислав
	 */
	public function beforeAction($action) {
		Yii::$app->response->format = Response::FORMAT_JSON;

		return parent::beforeAction($action);
	}

	/**
	 * Запрос населенного пункта OSM по названию
	 *
	 * @param string $q
	 * @param int    $level
	 * @param int    $limit
	 *
	 * @return array
	 *
	 * @throws \yii\db\Exception
	 *
	 * @author Исаков Владислав
	 */
	public function actionGetCity($q, $level = 6, $limit = 10) {
		$connection = Yii::$app->getDb();
		$command = $connection->createCommand("SELECT distinct(name) as title, osm_id as id, place as type
					FROM planet_osm_point
							 JOIN (
						SELECT ltrim(member, 'n')::bigint AS osm_id
						FROM (
								 SELECT unnest(members) AS member
								 FROM planet_osm_rels
								 WHERE ARRAY['boundary','administrative']<@tags  
								 AND (ARRAY ['admin_level','8'] <@ tags OR ARRAY ['admin_level','6'] <@ tags)) u
						WHERE member LIKE 'n%' ) x
								  USING(osm_id) where name ilike :name order by name limit :limit");

		$command->bindValue(':name', '%' . $q . '%');
		$command->bindValue(':limit', $limit);

		$result['results'] = $command->queryAll();

		return $result;
	}

	/**
	 * @param string $q
	 *
	 * @return mixed
	 *
	 * @author Исаков Владислав
	 */
	public function actionGetPhone($q) {
		$result['results'][] = ['id' => $q, 'title' => $q];

		return $result;
	}

	/**
	 * @param string $q
	 *
	 * @return mixed
	 *
	 * @author Исаков Владислав
	 */
	public function actionGetEmail($q) {
		$result['results'][] = ['id' => $q, 'title' => $q];

		return $result;
	}

	/**
	 * @param string $q
	 *
	 * @return mixed
	 *
	 * @author Исаков Владислав
	 */
	public function actionGetTag($q) {
		$result['results'][] = ['id' => $q, 'title' => $q];

		return $result;
	}

}