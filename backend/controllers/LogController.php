<?php

namespace backend\controllers;

use common\models\LogYii;
use common\modules\rbac\rules\Permissions;
use yii\filters\AccessControl;

/**
 * Class LogController
 *
 * @package backend\controllers
 *
 * @author  Исаков Владислав
 */
class LogController extends BackendController {
	const ACTION_INDEX = 'index';
	const ACTION_CLEAR = 'clear';

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow'   => true,
						'actions' => [
							static::ACTION_INDEX,
							static::ACTION_CLEAR,
						],
						'roles'   => [Permissions::ROLE_ADMIN],
					],
				],
			],
		];
	}

	/**
	 *
	 * @param int $status
	 *
	 * @return string
	 *
	 *
	 * @author Исаков Владислав
	 */
	public function actionIndex($status = null) {
		$this->view->title = 'Логи ошибок';

		$logs = LogYii::find();

		if ($status == 1) {
			$logs = $logs->andWhere([LogYii::ATTR_CATEGORY => 'yii\web\HttpException:403']);
		}

		if ($status == 2) {
			$logs = $logs->andWhere([LogYii::ATTR_CATEGORY => 'yii\web\HttpException:404']);
		}

		if ($status === null) {
			$logs = $logs->andWhere(['NOT IN', LogYii::ATTR_CATEGORY, ['yii\web\HttpException:403', 'yii\web\HttpException:404']]);
		}

		$logs = $logs->orderBy([LogYii::ATTR_LOG_TIME => SORT_DESC])
			->all();

		return $this->render('index', [
				'logs'   => $logs,
				'status' => $status
			]
		);
	}

	/**
	 * Очистка логов
	 *
	 * @author Исаков Владислав
	 */
	public function actionClear() {
		LogYii::deleteAll(null);

		return $this->redirect(static::getActionUrl(static::ACTION_INDEX));
	}
}
