<?php
/**
 * @author Исаков Владислав
 */

namespace backend\controllers;

use common\models\Place;
use common\modules\rbac\rules\Permissions;
use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class PlaceController extends BackendController {

	const ACTION_INDEX = '';
	const ACTION_EDIT = 'edit';

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'actions' => [
							static::ACTION_INDEX,
							static::ACTION_EDIT,
						],
						'allow'   => true,
						'roles'   => [Permissions::ROLE_ADMIN],
					],
				],
			],
		];
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав
	 */
	public function actionIndex() {
		$places = Place::find()->all();

		return $this->render('index', ['places' => $places]);
	}

	/**
	 * @param null|int $id
	 *
	 * @return string
	 *
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав
	 */
	public function actionEdit($id = null) {
		if (null === $id) {
			$model = new Place();
			$this->view->title = 'Новое место';
		}
		else {
			/** @var Place $model */
			$model = Place::find()
				->where([Place::ATTR_ID => $id])
				->one();

			if (null === $model) {
				throw new NotFoundHttpException('Место не найдено');
			}

			$this->view->title = 'Редактирование - ' . $model->title;
		}

		if (Yii::$app->request->isPost) {
			$model->load(Yii::$app->request->post());
			$model->coverFile = UploadedFile::getInstance($model, $model::ATTR_COVER_FILE);

			if (null !== $model->coverFile) {
				if (false === $model->uploadCover()) {
					$model->addError($model::ATTR_COVER_FILE, 'Ошибка загрузки изображения');
				}
			}

			if (!$model->hasErrors() && $model->validate()) {
				$model->tags = explode(',', str_replace(' ', '',$model->tags));
				$model->emails = explode(',', str_replace(' ', '', $model->emails));
				$model->phones = explode(',', str_replace(' ', '', $model->phones));
				$model->save(false);

				Yii::$app->session->addFlash('success', 'Сохранено');

				$this->redirect(static::getActionUrl(static::ACTION_EDIT, ['id' => $model->id]));
			} else {

				return $this->render('form', ['model' => $model]);
			}

			$model->tags = implode(', ', $model->tags);
			$model->emails = implode(', ', $model->emails);
			$model->phones = implode(', ', $model->phones);
		}
		else {
			$model->tags = implode(', ', $model->tags->getValue());
			$model->emails = implode(', ', $model->emails->getValue());
			$model->phones = implode(', ', $model->phones->getValue());
		}

		return $this->render('form', ['model' => $model]);
	}
}