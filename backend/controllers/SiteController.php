<?php

namespace backend\controllers;

use backend\models\FeedbackForm;
use backend\models\RegisterForm;
use ChurakovMike\Freekassa\forms\SuccessForm;
use common\exceptions\BadCallbackOrderException;
use common\models\LoginForm;
use common\models\Order;
use common\models\Tariff;
use common\models\TariffUser;
use common\models\User;
use Yii;
use yii\caching\TagDependency;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class SiteController extends BackendController {
	const ACTION_INDEX = 'index';
	const ACTION_LOGIN = 'login';
	const ACTION_LOGOUT = 'logout';
	const ACTION_ERROR = 'error';
	const ACTION_PROFILE = 'profile';
	const ACTION_FEEDBACK = 'feedback';
	const ACTION_REGISTER = 'register';
	const ACTION_DELETE_ORDER = 'delete-order';
	const ACTION_CREATE_ORDER = 'create-order';
	const ACTION_ORD_PAY = 'ord-pay';

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'actions' => [
							static::ACTION_LOGIN,
							static::ACTION_ERROR,
							static::ACTION_REGISTER,
							static::ACTION_FEEDBACK,
							static::ACTION_PROFILE,
							static::ACTION_DELETE_ORDER,
							static::ACTION_CREATE_ORDER,
							static::ACTION_ORD_PAY
						],
						'allow'   => true,
					],
					[
						'actions' => [
							static::ACTION_LOGOUT,
							static::ACTION_INDEX
						],
						'allow'   => true,
						'roles'   => ['@'],
					],
				],
			],
			'verbs'  => [
				'class'   => VerbFilter::class,
				'actions' => [
					static::ACTION_LOGOUT  => ['post', 'get'],
					static::ACTION_ORD_PAY => ['post'],
				],
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	/**
	 * Главная страница
	 *
	 * @return string
	 */
	public function actionIndex() {
		$this->view->title = 'События';

		return $this->render('index');
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав
	 */
	public function actionProfile() {
		$this->view->title = 'Профиль';

		/** @var User $user */
		$user = User::findOne([Yii::$app->user->id]);

		$userTariffs = $user->tariffsDetails;

		$orders = Order::find()
			->andWhere([Order::ATTR_USER_ID => Yii::$app->user->id])
			->all();

		$allActiveTariffs = [];
		if (count($userTariffs) === 0) {
			$allActiveTariffs = Tariff::findAll([Tariff::ATTR_ACTIVE => 1]);
		}

		return $this->render('profile', ['orders' => $orders, 'tariffs' => $userTariffs, 'allActiveTariffs' => $allActiveTariffs]);
	}

	/**
	 * Login action.
	 *
	 * @return string
	 */
	public function actionLogin() {
		$this->view->title = 'Вход';
		$this->layout = '/guest';

		if (!Yii::$app->user->isGuest) {
			return $this->redirect(static::getActionUrl(static::ACTION_PROFILE));
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->goBack();
		}
		else {
			$model->password = '';

			return $this->render('login', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Logout action.
	 *
	 * @return string
	 */
	public function actionLogout() {
		Yii::$app->user->logout();

		return $this->goHome();
	}

	/**
	 * @param int $t
	 *
	 * @return string|\yii\web\Response
	 *
	 * @author Исаков Владислав
	 */
	public function actionRegister($t = null) {
		if (!Yii::$app->user->isGuest) {
			return $this->redirect('/internal/profile/');
		}

		$this->view->title = 'Регистрация';
		$this->layout = '/guest';

		$model = new RegisterForm();
		if ($model->load(Yii::$app->request->post()) && $model->signup($t)) {
			Yii::$app->session->addFlash('success', 'Вы успешно зарегистрированы');

			Yii::$app->mailer->compose()
				->setFrom('help@t-gate.ru')
				->setTo(['help@t-gate.ru'])
				->setSubject('Зарегистрирован новый пользователь')
				->setHtmlBody($model->username . '<br>' . $model->email)
				->send();

			return $this->goHome();
		}

		return $this->render('register', [
			'model' => $model,
		]);
	}

	/**
	 * @return string|\yii\web\Response
	 *
	 * @author Исаков Владислав
	 */
	public function actionFeedback() {
		$this->view->title = 'Обратная связь';
		$this->layout = '/guest';

		$model = new FeedbackForm();
		if ($model->load(Yii::$app->request->post()) && $model->saveMessage()) {

			Yii::$app->session->addFlash('success', 'Спасибо за ваше сообщение!');

			return $this->redirect('/');
		}

		return $this->render('feedback', [
			'model' => $model,
		]);
	}

	/**
	 * Удаление неоплаченного заказа
	 *
	 * @param int $id
	 *
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав
	 */
	public function actionDeleteOrder($id) {
		$order = Order::find()
			->andWhere([Order::ATTR_USER_ID => Yii::$app->user->id])
			->andWhere([Order::ATTR_ID => $id])
			->andWhere([Order::ATTR_STATUS => Order::STATUS_NOT_PAYED])
			->one();

		if (null === $order) {
			throw new NotFoundHttpException('Заказ не найден');
		}

		$order->delete();

		$this->redirect(SiteController::getActionUrl(SiteController::ACTION_PROFILE));
	}

	/**
	 * Создание заказа на подключение тарифа
	 *
	 * @param int $t
	 *
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав
	 */
	public function actionCreateOrder($t) {
		$tariff = Tariff::findOne([Tariff::ATTR_ACTIVE => 1]);

		if (null === $tariff) {
			throw  new NotFoundHttpException('Тариф не найден');
		}

		$order = new Order();
		$order->user_id = Yii::$app->user->id;
		$order->tariff_id = $t;
		$order->status = Order::STATUS_NOT_PAYED;
		$order->save();
		$order->setNum($tariff);

		$this->redirect(SiteController::getActionUrl(SiteController::ACTION_PROFILE));
	}


	/**
	 * Обработка платежа
	 *
	 * @throws \common\exceptions\BadCallbackOrderException
	 *
	 * @author Исаков Владислав
	 */
	public function actionOrdPay() {
		$this->layout = false;

		$form = new SuccessForm();
		$form->setAttributes(Yii::$app->request->post());

		if ($form->validate()) {
			/** @var Order $order */
			$order = Order::find()
				->andWhere([Order::ATTR_NUMBER => $form->merchant_order_id])
				->andWhere([Order::ATTR_STATUS => Order::STATUS_NOT_PAYED])
				->one();

			Yii::$app->mailer->compose()
				->setFrom('robot@t-gate.ru')
				->setTo(['help@t-gate.ru'])
				->setSubject('Ошибка кэлбэка оплаты')
				->setHtmlBody('Не найден заказ для оплаты: ' . $form->merchant_order_id . '<br> Номер оплаты: ' . $form->intid)
				->send();

			if (null === $order) {
				throw new BadCallbackOrderException();
			}

			$order->status = Order::STATUS_PAYED;
			$order->payment_stamp = new Expression('current_timestamp');
			$order->kassa_id = $form->intid;
			$order->save();

			//удалим триальный тариф у пользователя
			TariffUser::deleteAll([TariffUser::ATTR_USER_ID => $order->user_id, TariffUser::ATTR_TARIFF_ID => Tariff::TRIAL_ID]);

			$tariffUser = new TariffUser();
			$tariffUser->tariff_id = $order->tariff_id;
			$tariffUser->user_id = $order->user_id;
			$tariffUser->begin_stamp = new Expression('current_timestamp');
			$tariffUser->end_stamp = new Expression('current_timestamp + interval \'31 day\'');
			$tariffUser->save();

			Yii::$app->mailer->compose()
				->setFrom('robot@t-gate.ru')
				->setTo(['help@t-gate.ru'])
				->setSubject('Пришла оплата')
				->setHtmlBody('Заказ оплачен: ' . $form->merchant_order_id . '<br> Номер оплаты: ' . $form->intid)
				->send();
		}
		else {
			Yii::$app->mailer->compose()
				->setFrom('robot@t-gate.ru')
				->setTo(['help@t-gate.ru'])
				->setSubject('Ошибка кэлбэка оплаты')
				->setHtmlBody('Коллбэк оплаты не прошел валидацию. <br> Заказ: ' . $form->merchant_order_id . '<br> Номер оплаты: ' . $form->intid)
				->send();

			throw new BadCallbackOrderException('Коллбэк оплаты не прошел валидацию');
		}

		TagDependency::invalidate(Yii::$app->cache, TariffUser::class);

		echo 'YES';
	}
}
