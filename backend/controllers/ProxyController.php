<?php

namespace backend\controllers;

use common\models\Proxy;
use common\modules\rbac\rules\Permissions;
use Yii;
use yii\caching\TagDependency;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * Class ProxyController
 *
 * @package backend\controllers
 *
 * @author  Исаков Владислав
 */
class ProxyController extends BackendController {
	const ACTION_INDEX = 'index';
	const ACTION_CLEAR = 'clear';
	const ACTION_EDIT = 'edit';
	const ACTION_CHANGE_STATUS = 'change-status';
	const ACTION_DELETE = 'delete';

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow'   => true,
						'actions' => [
							static::ACTION_INDEX,
							static::ACTION_CLEAR,
							static::ACTION_EDIT,
							static::ACTION_CHANGE_STATUS,
							static::ACTION_DELETE,
						],
						'roles'   => [Permissions::ROLE_ADMIN],
					],
				],
			],
		];
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав
	 */
	public function actionIndex() {
		$this->view->title = 'Прокси серверы';

		$proxyList = Proxy::findAll([Proxy::ATTR_ACTIVE => true]);
		$proxyBlockedList = Proxy::findAll([Proxy::ATTR_ACTIVE => false]);
		if (false === $proxyList) {
			$proxyList = [];
		}
		if (false === $proxyBlockedList) {
			$proxyBlockedList = [];
		}

		return $this->render('index', ['proxyList' => $proxyList, 'proxyBlockedList' => $proxyBlockedList]);
	}

	/**
	 * @param int $id
	 *
	 * @return \yii\web\Response
	 *
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав
	 */
	public function actionChangeStatus($id) {
		$proxy = Proxy::findOne($id);

		if (null === $proxy) {
			throw new NotFoundHttpException('Прокси не найден');
		}

		$proxy->active = !$proxy->active;

		$proxy->save();

		return $this->redirect(static::getActionUrl(static::ACTION_INDEX));
	}

	/**
	 * Очистка кэша прокси
	 *
	 * @author Исаков Владислав
	 */
	public function actionClear() {
		TagDependency::invalidate(Yii::$app->cache, [Proxy::class]);

		return $this->redirect(static::getActionUrl(static::ACTION_INDEX));
	}

	/**
	 * @param null|int $id
	 *
	 * @return string|\yii\web\Response
	 *
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав
	 */
	public function actionEdit($id = null) {
		$this->view->title = 'Редактрование прокси-серверов';
		$model = new Proxy();

		if (Yii::$app->request->isPost) {
			$model->load(Yii::$app->request->post());

			if ($model->save()) {
				TagDependency::invalidate(Yii::$app->cache, [Proxy::class]);

				return $this->redirect(static::getActionUrl(static::ACTION_INDEX));
			}
		}
		else {
			if (null !== $id) {
				$model = Proxy::findOne($id);

				if (null === $model) {
					throw new NotFoundHttpException('Прокси не найден');
				}
			}
		}

		return $this->render('edit', ['model' => $model]);
	}

	/**
	 * @param int $id
	 *
	 * @return \yii\web\Response
	 *
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав
	 */
	public function actionDelete($id) {
		$model = Proxy::find()->andWhere([Proxy::ATTR_ID => $id])
			->one();

		if (null === $model) {
			throw new NotFoundHttpException('Прокси не найден');
		}

		$model->delete();
		TagDependency::invalidate(Yii::$app->cache, [Proxy::class]);

		return $this->redirect(static::getActionUrl(static::ACTION_INDEX));
	}
}
