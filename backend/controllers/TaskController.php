<?php
namespace backend\controllers;

use common\models\ObjectFile;
use common\models\Task;
use common\models\User;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * Class TaskController
 *
 * @package backend\controllers
 *
 * @author  Исаков Владислав
 */
class TaskController extends BackendController {

	const ACTION_INDEX = '';
	const ACTION_EDIT = 'edit';
	const ACTION_DELETE = 'delete';
	const ACTION_GET_FILE = 'get-file';
	const ACTION_RENEW = 're-new';

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'actions' => [
							static::ACTION_EDIT,
							static::ACTION_INDEX,
							static::ACTION_DELETE,
							static::ACTION_GET_FILE,
							static::ACTION_RENEW,
							'index'
						],
						'allow'   => true,
						'roles'   => ['@'],
					],
				],
			],
		];
	}

	/**
	 * @param null $status
	 *
	 * @return string
	 *
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав
	 */
	public function actionIndex($status = null) {
		$this->view->title = 'Менеджер задач';

		if ($status !== null && !array_key_exists($status, Task::STATUS_NAMES)) {
			throw new NotFoundHttpException('Неизвестный статус задачи');
		}

		$tasks = Task::find()
			->andWhere([Task::ATTR_INSERT_USER => Yii::$app->user->id]);

		if ($status !== null) {
			$tasks->andWhere([Task::ATTR_STATUS => $status]);
		}

		$tasks = $tasks->orderBy([Task::ATTR_UPDATE_STAMP => SORT_DESC])
			->all();

		/** @var User $user */
		$user = User::findOne([Yii::$app->user->id]);

		$userTariffs = $user->tariffsDetails;

		return $this->render('index', ['tasks' => $tasks, 'status' => $status, 'userTariffs' => $userTariffs]);
	}

	/**
	 * @param null $id
	 *
	 * @return string|\yii\web\Response
	 *
	 * @throws \yii\base\Exception
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав
	 */
	public function actionEdit($id = null) {
		/** @var User $user */
		$user = User::findOne([Yii::$app->user->id]);

		if (count($user->tariffsDetails) === 0) {
			throw new Exception('Нет активных тарифов для добавления задач');
		}

		$this->view->title = 'Менеджер задач';

		$model = new Task();

		if (null !== $id) {
			$model = Task::find()->andWhere([Task::ATTR_ID => $id])
				->andWhere([Task::ATTR_INSERT_USER => Yii::$app->user->id])
				->one();

			if (null === $model) {
				throw new NotFoundHttpException('Задача не найдена');
			}
		}

		if (Yii::$app->request->isPost) {
			$model->load(Yii::$app->request->post());
			if ($model->save()) {
				Yii::$app->session->addFlash('success', 'Задача добавлена в очередь');

				return $this->redirect(static::getActionUrl(static::ACTION_INDEX));
			}
			else {
				Yii::$app->session->addFlash('danger', 'Произошла ошибка');
			}
		}

		return $this->render('edit', ['model' => $model]);
	}

	/**
	 * @param int $id
	 *
	 * @return \yii\web\Response
	 *
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав
	 */
	public function actionDelete($id) {
		$model = Task::find()->andWhere([Task::ATTR_ID => $id])
			->andWhere([Task::ATTR_INSERT_USER => Yii::$app->user->id])
			->one();

		if (null === $model) {
			throw new NotFoundHttpException('Задача не найдена');
		}

		$model->delete();

		return $this->redirect(static::getActionUrl(static::ACTION_INDEX));
	}


	/**
	 * @param int $taskId
	 *
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав
	 */
	public function actionReNew($taskId) {
		/** @var User $user */
		$user = User::findOne([Yii::$app->user->id]);

		if (count($user->tariffsDetails) === 0) {
			throw new Exception('Нет активных тарифов для добавления задач');
		}

		/** @var Task $task */
		$task = Task::find()
			->andWhere([Task::ATTR_INSERT_USER => Yii::$app->user->id])
			->andWhere([Task::ATTR_ID => $taskId])
			->one();

		if (null === $task) {
			throw new NotFoundHttpException('Задача не найдена');
		}
		/** @var ObjectFile[] $files */
		$files = ObjectFile::find()
			->andWhere([ObjectFile::ATTR_OBJECT => Task::class])
			->andWhere([ObjectFile::ATTR_OBJECT_ID => $taskId])
			->all();

		foreach ($files as $file) {
			@unlink(Yii::getAlias('@files') . DIRECTORY_SEPARATOR . $file->filename);
			$file->delete();
		}

		$task->status = Task::STATUS_IN_QUEUE;
		$task->completed = 0;
		$task->save();
	}

	/**
	 * @param $fileName
	 *
	 * @return \yii\web\Response
	 *
	 * @author Исаков Владислав
	 */
	public function actionGetFile($fileName) {
		return $this->redirect('/files/' . $fileName);
	}
}