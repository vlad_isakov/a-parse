<?php

namespace backend\controllers;

use common\base\helpers\StringHelper;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class BackendController extends Controller {

	/**
	 * @param \yii\base\Action $action
	 *
	 * @return bool|void
	 *
	 * @throws \yii\web\BadRequestHttpException
	 *
	 * @author Исаков Владислав
	 */
	public function beforeAction($action) {
		if (Yii::$app->user->isGuest && ($action->id !== 'login' && $action->id !== 'register' && $action->id !== 'feedback')) {
			return Yii::$app->getResponse()->redirect(['site/login'])->send();
		}

		if ($action->id == SiteController::ACTION_ORD_PAY) {
			Yii::$app->request->enableCsrfValidation = false;
		}

		return parent::beforeAction($action);
	}

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => false, //Запрещаем все действия по-умолчанию, чтобы потом разрешать в дочерних контроллерах
					],
				],
			],
		];
	}

	/**
	 * Получение ссылки на указанное действие исходя из контроллера.
	 *
	 * @param string $actionName   Название действия
	 * @param array  $actionParams Дополнительные параметры
	 *
	 *
	 * @return string
	 * @author Isakov Vladislav
	 *
	 */
	public static function getActionUrl($actionName, array $actionParams = []) {
		$prefix = null;

		$moduleName = preg_replace('/^.*\\\\modules\\\\(.*?)\\\\.*$/', '\1', static::class);
		$controllerName = preg_replace('/Controller$/', '', StringHelper::basename(static::class));
		$controllerName = mb_strtolower(preg_replace('~(?!\b)([A-Z])~', '-\\1', $controllerName)); // Преобразуем название контроллера к формату url (aaa-bbb-ccc-..)
		if (false !== strpos($moduleName, 'backend')) {
			$moduleName = '';
		}

		$actionParams[0] = implode('/', [
			$moduleName,
			$controllerName,
			$actionName,
		]);

		$actionParams[0] = '/' . $prefix . $actionParams[0];

		/** @var string $url */
		$url = Yii::$app->urlManager->createUrl($actionParams);

		return $url;
	}
}
