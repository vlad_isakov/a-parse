<?php

namespace backend\controllers;

use backend\models\RouteSearchForm;
use common\models\EveTradeRoute;
use common\models\Proxy;
use common\modules\rbac\rules\Permissions;
use Yii;
use yii\caching\TagDependency;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * Class ProxyController
 *
 * @package backend\controllers
 *
 * @author  Исаков Владислав
 */
class EveController extends BackendController {
	const ACTION_INDEX = 'index';

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow'   => true,
						'actions' => [
							static::ACTION_INDEX,
						],
						'roles'   => ['@'],
					],
				],
			],
		];
	}

	/**
	 * @return string
	 *
	 * @author Исаков Владислав
	 */
	public function actionIndex() {
		$this->view->title = 'EVE Trade Routes';
        $form = new RouteSearchForm();
        $form->search();

		return $this->render('index', ['form' => $form, 'pages' => $form->pages]);
	}

	/**
	 * @param int $id
	 *
	 * @return \yii\web\Response
	 *
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав
	 */
	public function actionChangeStatus($id) {
		$proxy = Proxy::findOne($id);

		if (null === $proxy) {
			throw new NotFoundHttpException('Прокси не найден');
		}

		$proxy->active = !$proxy->active;

		$proxy->save();

		return $this->redirect(static::getActionUrl(static::ACTION_INDEX));
	}

	/**
	 * Очистка кэша прокси
	 *
	 * @author Исаков Владислав
	 */
	public function actionClear() {
		TagDependency::invalidate(Yii::$app->cache, [Proxy::class]);

		return $this->redirect(static::getActionUrl(static::ACTION_INDEX));
	}

	/**
	 * @param null|int $id
	 *
	 * @return string|\yii\web\Response
	 *
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав
	 */
	public function actionEdit($id = null) {
		$this->view->title = 'Редактрование прокси-серверов';
		$model = new Proxy();

		if (Yii::$app->request->isPost) {
			$model->load(Yii::$app->request->post());

			if ($model->save()) {
				TagDependency::invalidate(Yii::$app->cache, [Proxy::class]);

				return $this->redirect(static::getActionUrl(static::ACTION_INDEX));
			}
		}
		else {
			if (null !== $id) {
				$model = Proxy::findOne($id);

				if (null === $model) {
					throw new NotFoundHttpException('Прокси не найден');
				}
			}
		}

		return $this->render('edit', ['model' => $model]);
	}

	/**
	 * @param int $id
	 *
	 * @return \yii\web\Response
	 *
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 * @throws \yii\web\NotFoundHttpException
	 *
	 * @author Исаков Владислав
	 */
	public function actionDelete($id) {
		$model = Proxy::find()->andWhere([Proxy::ATTR_ID => $id])
			->one();

		if (null === $model) {
			throw new NotFoundHttpException('Прокси не найден');
		}

		$model->delete();
		TagDependency::invalidate(Yii::$app->cache, [Proxy::class]);

		return $this->redirect(static::getActionUrl(static::ACTION_INDEX));
	}
}
