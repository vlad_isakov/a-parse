<?php
/**
 * @author Исаков Владислав
 *
 * @var \common\models\Place[] $places
 */

use backend\controllers\PlaceController;

?>
<a class="btn btn-primary" href="<?= PlaceController::getActionUrl(PlaceController::ACTION_EDIT) ?>">Добавить</a>
