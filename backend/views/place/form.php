<?php

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use kartik\select2\Select2;
use yii\web\JsExpression;
use backend\controllers\AjaxController;
use kartik\file\FileInput;
use kartik\icons\FontAwesomeAsset;

FontAwesomeAsset::register($this);

/**
 * @author Исаков Владислав
 *
 *
 * @var \common\models\Place $model
 * @var \yii\web\View        $this
 */
$this->params['breadcrumbs'][] = ['label' => 'Места', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="col-md-12">
	<div class="card">
		<div class="card-header card-header-success">
			<h4 class="card-title">
				<?= ($model->isNewRecord ? 'Заполните необходимые данные' : $model->title) ?>
			</h4>
		</div>
		<div class="card-body">
			<?php $htmlForm = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
			<?= $htmlForm->field($model, $model::ATTR_TITLE)->textInput(['maxlength' => true]) ?>

			<?= $htmlForm->field($model, $model::ATTR_DESCRIPTION)->textarea(['maxlength' => true, 'rows' => 7]) ?>
			<div class="row">
				<div class="col-md-4 col-sm-12">
					<?= $htmlForm->field($model, $model::ATTR_TYPE)->dropDownList($model::TYPE_NAMES) ?>
				</div>
				<div class="col-md-4 col-sm-12">
					<?=
					$htmlForm->field($model, $model::ATTR_CITY_ID)->widget(Select2::class, [
						'model'         => $model,
						'attribute'     => $model::ATTR_CITY_ID,
						'data' => ['1215862324' => 'sdssdsd'],
						'theme'         => Select2::THEME_MATERIAL,
						'options'       => [
							'multiple' => false,
							'prompt'   => 'Нет',
						],
						'pluginOptions' => [
							'placeholder'        => 'Город...',
							'ajax'               => [
								'url'      => AjaxController::getActionUrl(AjaxController::ACTION_GET_CITY),
								'dataType' => 'json',
								'data'     => new JsExpression('function(params) { return {q:params.term}; }')
							],
							'escapeMarkup'       => new JsExpression('function (markup) { return markup; }'),
							'templateResult'     => new JsExpression(' function (data) {
                     if (data.type == "devider") { 
                        return data.title; 
                     }
                     
                     return data.title;
                }'),
							'templateSelection'  => new JsExpression('function (data) { return data.title; }'),
							'allowClear'         => true,
							'minimumInputLength' => 3,
						],
					]);
					?>
				</div>
				<div class="col-md-4 col-sm-12">
					<?=
					$htmlForm->field($model, $model::ATTR_NEAREST_LOCATION_ID)->widget(Select2::class, [
						'model'         => $model,
						'attribute'     => $model::ATTR_NEAREST_LOCATION_ID,
						'theme'         => Select2::THEME_MATERIAL,
						'options'       => [
							'multiple' => false,
							'prompt'   => 'Нет',
						],
						'pluginOptions' => [
							'placeholder'        => 'Город...',
							'ajax'               => [
								'url'      => AjaxController::getActionUrl(AjaxController::ACTION_GET_CITY),
								'dataType' => 'json',
								'data'     => new JsExpression('function(params) { return {q:params.term}; }')
							],
							'escapeMarkup'       => new JsExpression('function (markup) { return markup; }'),
							'templateResult'     => new JsExpression(' function (data) {
                     if (data.type == "devider") { 
                        return data.title; 
                     }
                     
                     return data.title;
                }'),
							'templateSelection'  => new JsExpression('function (data) { return data.title; }'),
							'allowClear'         => true,
							'minimumInputLength' => 3,
						],
					]);
					?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2 col-sm-6">
					<?= $htmlForm->field($model, $model::ATTR_LAT)->textInput(['maxlength' => true]) ?>
				</div>
				<div class="col-md-2 col-sm-6">
					<?= $htmlForm->field($model, $model::ATTR_LON)->textInput(['maxlength' => true]) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<?= $htmlForm->field($model, $model::ATTR_EMAILS)->textInput() ?>
				</div>
				<div class="col-md-6 col-sm-12">
					<?= $htmlForm->field($model, $model::ATTR_PHONES)->textInput() ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<?= $htmlForm->field($model, $model::ATTR_ADDRESS)->textInput() ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-12">
					<?= $htmlForm->field($model, $model::ATTR_TWITTER)->textInput() ?>
				</div>
				<div class="col-md-4 col-sm-12">
					<?= $htmlForm->field($model, $model::ATTR_INSTAGRAM)->textInput() ?>
				</div>
				<div class="col-md-4 col-sm-12">
					<?= $htmlForm->field($model, $model::ATTR_WEB_SITE)->textInput() ?>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<?= $htmlForm->field($model, $model::ATTR_TAGS)->textInput() ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<?= $htmlForm->field($model, $model::ATTR_COVER_FILE)->widget(FileInput::class, [
						'options'       => ['accept' => 'image/*'],
						'pluginOptions' => [
							'showUpload' => false
						]
					]); ?>
				</div>
				<div class="form-group">
					<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
				</div>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>


