<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use himiklab\yii2\recaptcha\ReCaptcha2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div class="card">
	<div class="card-body">
		<div class="site-login">
			<h3><img src="/images/logo.png" alt="T-Gate Soft"/> / Личный кабинет</h3>
			<div class="row" style="margin-top: 40px;">
				<div class="col-lg-6">
					<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

					<?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

					<?= $form->field($model, 'password')->passwordInput() ?>

					<?= $form->field($model, 'rememberMe')->checkbox() ?>

					<?= $form->field($model, 'reCaptcha')->widget(
						ReCaptcha2::class
					)->label(false) ?>
					<div class="form-group">
						<?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
					</div>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
