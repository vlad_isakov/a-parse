<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */

/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">
	<h2><?= Html::encode($this->title) ?></h2>
	<div class="alert alert-danger">
		<?= nl2br(Html::encode($message)) ?>
	</div>
	<p>
		Случилось что-то нехорошее.
	</p>
	<p>
		Пожалуйста, <a href='/internal/feedback/'>сообщите</a> нам, если вы считаете, что это ошибка. Спасибо.
	</p>
</div>
