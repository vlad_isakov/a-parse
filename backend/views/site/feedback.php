<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \backend\models\FeedbackForm */

use himiklab\yii2\recaptcha\ReCaptcha2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div class="card">
	<div class="card-body">
		<div class="site-register">
			<h3><img src="/images/logo.png" alt="T-Gate Soft"/> / Обратная связь</h3>
			<div class="row" style="margin-top: 40px;">
				<div class="col-lg-6">
					<?php $form = ActiveForm::begin(['id' => 'feedback-form']); ?>

					<?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => 'Имя'])->label(false) ?>

					<?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->label(false) ?>

					<?= $form->field($model, 'text')->textarea(['placeholder' => 'Сообщение'])->label(false) ?>

					<?= $form->field($model, 'reCaptcha')->widget(
						ReCaptcha2::class
					)->label(false) ?>
					<div class="form-group">
						<?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
					</div>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

