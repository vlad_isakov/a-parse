<?php

use backend\controllers\SiteController;
use common\models\Order;
use common\models\Tariff;

/**
 * @var yii\web\View                $this
 * @var \common\models\Order[]      $orders
 * @var \common\models\TariffUser[] $tariffs
 * @var \common\models\Tariff[]     $allActiveTariffs
 *
 */
?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header card-header-tabs card-header-primary">
				<div class="nav-tabs-navigation">
					<div class="nav-tabs-wrapper">
						<ul class="nav nav-tabs">
							<strong><?= (count($orders) > 0 ? 'Заказы' : 'Тарифы') ?></strong>
						</ul>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="tab-content">
					<div class="tab-pane active show">
						<?php if (count($orders) === 0): ?>
							<div class="row">
								<?php foreach ($allActiveTariffs as $tariff): ?>
									<div class="col-md-4">
										<div class="card">
											<div class="card-header card-header-tabs card-header-success" style="font-size: 18px;">
												<strong> <?= $tariff->title ?></strong><strong class="pull-right"><?= $tariff->cost ?> &#8381;</strong>
											</div>
											<div class="card-body text-center">
												<p><strong><?= $tariff->count ?></strong> объявлений в день</p>
												<?php if ($tariff->may_is_phone): ?>
													<p><strong>Парсинг телефонов</strong></p>
												<?php endif ?>
												<?php if ($tariff->may_is_repeat): ?>
													<p><strong>Автоматическое повторение</strong></p>
												<?php endif ?>
												<?php if ($tariff->may_is_send_to_email): ?>
													<p><strong>Отправка файлов на почту</strong></p>
												<?php endif ?>
												<a class="btn btn-primary" href="<?= SiteController::getActionUrl(SiteController::ACTION_CREATE_ORDER, ['t' => $tariff->id]) ?>">Подключить</a>
											</div>
										</div>
									</div>
								<?php endforeach ?>
							</div>
						<?php else: ?>
							<table class="table table-hover">
								<thead class="thead-light">
								<tr>
									<th>Тариф</th>
									<th>Дата</th>
									<th>Сатус</th>
									<th></th>
									<th></th>
								</tr>
								</thead>
								<?php foreach ($orders as $order): ?>
									<tr>
										<td><strong><?= $order->tariff->title ?></strong></td>
										<td><strong><?= $order->insert_stamp ?></strong></td>
										<td><strong><?= Order::STATUS_NAMES[$order->status] ?></strong></td>
										<td class="text-center">
											<?php if ($order->status !== Order::STATUS_PAYED): ?>
												<a class="btn btn-primary" href="https://www.free-kassa.ru/merchant/cash.php?oa=<?= $order->tariff->cost ?>&o=<?= $order->number ?>&us_desc=&s=28485dc04c1f2d26ee54e52e7ad3ad14&m=201293">Оплатить</a>
											<?php endif ?>
										</td>
										<td class="text-center alert-danger">
											<?php if ($order->status !== Order::STATUS_PAYED): ?>
												<a href="<?= SiteController::getActionUrl(SiteController::ACTION_DELETE_ORDER, ['id' => $order->id]) ?>"><i class="material-icons">clear</i></a>
											<?php endif ?>
										</td>
									</tr>
								<?php endforeach ?>
							</table>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if (count($tariffs) > 0): ?>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header card-header-tabs card-header-primary">
					<div class="nav-tabs-navigation">
						<div class="nav-tabs-wrapper">
							<ul class="nav nav-tabs">
								<strong>Действующие тарифы</strong>
							</ul>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="tab-content">
						<div class="tab-pane active show">
							<table class="table table-hover">
								<thead class="thead-light">
								<tr>
									<th>Тариф</th>
									<th>Сайт</th>
									<th>Дата окончания</th>
									<th>Кол-во в день</th>
								</tr>
								</thead>
								<?php foreach ($tariffs as $tariff): ?>
									<tr>
										<td><strong><?= $tariff->tariff->title ?></strong></td>
										<td><strong><?= Tariff::SOURCE_NAMES[$tariff->tariff->source_id] ?></strong></td>
										<td><strong><?= $tariff->end_stamp ?></strong></td>
										<td><strong><?= $tariff->tariff->count ?></strong></td>
									</tr>
								<?php endforeach ?>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif ?>


