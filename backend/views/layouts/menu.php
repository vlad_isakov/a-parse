<?php

use backend\controllers\EveController;
use backend\controllers\LogController;
use backend\controllers\ProxyController;
use backend\controllers\SiteController;
use backend\controllers\TaskController;
use common\modules\rbac\rules\Permissions;

?>

<ul class="nav">
	<li class="nav-item <?= (strpos(Yii::$app->request->url, SiteController::getActionUrl(SiteController::ACTION_PROFILE)) === false ? '' : 'active') ?>">
		<a class="nav-link" href="<?= SiteController::getActionUrl(SiteController::ACTION_PROFILE) ?>">
			<i class="material-icons">person</i>
			<p>Профиль</p>
		</a>
	</li>
	<li class="nav-item <?= (strpos(Yii::$app->request->url, TaskController::getActionUrl(TaskController::ACTION_INDEX)) === false ? '' : 'active') ?>">
		<a class="nav-link" href="<?= TaskController::getActionUrl(TaskController::ACTION_INDEX) ?>">
			<i class="material-icons">content_paste</i>
			<p>Задачи</p>
		</a>
	</li>
	<?php if (Yii::$app->user->can(Permissions::P_ADMIN)): ?>
		<hr>
		<li class="nav-item <?= (strpos(Yii::$app->request->url, LogController::getActionUrl(LogController::ACTION_INDEX)) === false ? '' : 'active') ?>">
			<a class="nav-link" href="<?= LogController::getActionUrl(LogController::ACTION_INDEX) ?>">
				<i class="material-icons">announcement</i>
				<p>Логи ошибок</p>
			</a>
		</li>
		<li class="nav-item <?= (strpos(Yii::$app->request->url, ProxyController::getActionUrl(ProxyController::ACTION_INDEX)) === false ? '' : 'active') ?>">
			<a class="nav-link" href="<?= ProxyController::getActionUrl(ProxyController::ACTION_INDEX) ?>">
				<i class="material-icons">security</i>
				<p>Прокси</p>
			</a>
		</li>
		<li class="nav-item <?= (strpos(Yii::$app->request->url, EveController::getActionUrl(EveController::ACTION_INDEX)) === false ? '' : 'active') ?>">
			<a class="nav-link" href="<?= EveController::getActionUrl(EveController::ACTION_INDEX) ?>">
				<i class="material-icons">list</i>
				<p>EVE</p>
			</a>
		</li>
	<?php endif ?>
</ul>