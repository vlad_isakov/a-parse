<?php
/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\AppAsset;
use backend\controllers\SiteController;
use backend\widgets\NotificationWidget;

AppAsset::register($this);
?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<link rel="apple-touch-icon" sizes="76x76" href="/internal/assets/img/apple-icon.png">
	<link rel="icon" type="image/png" href="/internal/assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	<title>
		<?= $this->title . ' - T-Gate Soft' ?>
	</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
	<!--     Fonts and icons     -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
	<!-- CSS Files -->
	<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<!-- CSS Just for demo purpose, don't include it in your project -->
	<?php $this->registerCsrfMetaTags() ?>
	<?php $this->head() ?>
</head>

<body class="">
<?php $this->beginBody() ?>
<div class="wrapper ">
	<div class="sidebar" data-color="purple" data-background-color="white" data-image="/internal/assets/img/sidebar-1.jpg">
		<div class="logo">
			<a href="/" class="simple-text logo-normal">
				<img src="/images/logo.png" alt="T-Gate Soft"/>
			</a>
		</div>
		<div class="sidebar-wrapper">
			<?= $this->render('menu') ?>
		</div>
	</div>
	<div class="main-panel">
		<!-- Navbar -->
		<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
			<div class="container-fluid">
				<div class="navbar-wrapper">
					<strong><?= $this->title ?></strong>
				</div>
				<button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
					<span class="sr-only">Toggle navigation</span>
					<span class="navbar-toggler-icon icon-bar"></span>
					<span class="navbar-toggler-icon icon-bar"></span>
					<span class="navbar-toggler-icon icon-bar"></span>
				</button>
				<div class="collapse navbar-collapse justify-content-end">
					<form class="navbar-form">
						<div class="input-group no-border">

						</div>
					</form>
					<ul class="navbar-nav">
						<?= NotificationWidget::widget() ?>
						<li class="nav-item dropdown">
							<a class="nav-link" href="<?= SiteController::getActionUrl(SiteController::ACTION_PROFILE) ?>" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="material-icons">person</i>
								<p class="d-lg-none d-md-block">
									Профиль
								</p>
							</a>
							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
								<a class="dropdown-item" href="<?= SiteController::getActionUrl(SiteController::ACTION_PROFILE) ?>">Профиль</a>
								<div class="dropdown-divider"></div>
								<?php if (!Yii::$app->user->isGuest): ?>
									<a class="dropdown-item" href="/internal/logout/">Выйти(<?= Yii::$app->user->identity->username ?>)</a>
								<?php endif ?>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- End Navbar -->
		<div class="content">
			<div class="container-fluid">
				<?= $content ?>
			</div>
		</div>
		<footer class="footer">
			<div class="container-fluid">
				<nav class="float-left">
					<ul>
						<li>
							<a href="/">
								Главная
							</a>
						</li>
						<li>
							<a href="/internal/feedback/">
								Обратная связь
							</a>
						</li>
					</ul>
				</nav>
				<div class="copyright float-right">
					&copy;
					<script>
						document.write(new Date().getFullYear())
					</script>
				</div>
			</div>
		</footer>
	</div>
</div>
<!--   Core JS Files   -->

<script src="/internal/assets/js/core/popper.min.js"></script>
<script src="/internal/assets/js/core/bootstrap-material-design.min.js"></script>
<script src="/internal/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Plugin for the momentJs  -->
<script src="/internal/assets/js/plugins/moment.min.js"></script>
<!--  Plugin for Sweet Alert -->
<script src="/internal/assets/js/plugins/sweetalert2.js"></script>
<!-- Forms Validations Plugin -->
<script src="/internal/assets/js/plugins/jquery.validate.min.js"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="/internal/assets/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="/internal/assets/js/plugins/bootstrap-selectpicker.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="/internal/assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="/internal/assets/js/plugins/jquery.dataTables.min.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="/internal/assets/js/plugins/bootstrap-tagsinput.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="/internal/assets/js/plugins/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="/internal/assets/js/plugins/fullcalendar.min.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="/internal/assets/js/plugins/jquery-jvectormap.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="/internal/assets/js/plugins/nouislider.min.js"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding dinamically elements -->
<script src="/internal/assets/js/plugins/arrive.min.js"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chartist JS -->
<script src="/internal/assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="/internal/assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="/internal/assets/js/material-dashboard.js?v=2.1.1" type="text/javascript"></script>

<script>
	$(document).ready(function () {
		$().ready(function () {
			$sidebar = $('.sidebar');

			$sidebar_img_container = $sidebar.find('.sidebar-background');

			$full_page = $('.full-page');

			$sidebar_responsive = $('body > .navbar-collapse');

			window_width = $(window).width();

			fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

			if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
				if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
					$('.fixed-plugin .dropdown').addClass('open');
				}

			}

			$('.fixed-plugin a').click(function (event) {
				// Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
				if ($(this).hasClass('switch-trigger')) {
					if (event.stopPropagation) {
						event.stopPropagation();
					} else if (window.event) {
						window.event.cancelBubble = true;
					}
				}
			});

			$('.fixed-plugin .active-color span').click(function () {
				$full_page_background = $('.full-page-background');

				$(this).siblings().removeClass('active');
				$(this).addClass('active');

				var new_color = $(this).data('color');

				if ($sidebar.length != 0) {
					$sidebar.attr('data-color', new_color);
				}

				if ($full_page.length != 0) {
					$full_page.attr('filter-color', new_color);
				}

				if ($sidebar_responsive.length != 0) {
					$sidebar_responsive.attr('data-color', new_color);
				}
			});

			$('.fixed-plugin .background-color .badge').click(function () {
				$(this).siblings().removeClass('active');
				$(this).addClass('active');

				var new_color = $(this).data('background-color');

				if ($sidebar.length != 0) {
					$sidebar.attr('data-background-color', new_color);
				}
			});

			$('.fixed-plugin .img-holder').click(function () {
				$full_page_background = $('.full-page-background');

				$(this).parent('li').siblings().removeClass('active');
				$(this).parent('li').addClass('active');


				var new_image = $(this).find("img").attr('src');

				if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
					$sidebar_img_container.fadeOut('fast', function () {
						$sidebar_img_container.css('background-image', 'url("' + new_image + '")');
						$sidebar_img_container.fadeIn('fast');
					});
				}

				if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
					var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

					$full_page_background.fadeOut('fast', function () {
						$full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
						$full_page_background.fadeIn('fast');
					});
				}

				if ($('.switch-sidebar-image input:checked').length == 0) {
					var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
					var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

					$sidebar_img_container.css('background-image', 'url("' + new_image + '")');
					$full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
				}

				if ($sidebar_responsive.length != 0) {
					$sidebar_responsive.css('background-image', 'url("' + new_image + '")');
				}
			});

			$('.switch-sidebar-image input').change(function () {
				$full_page_background = $('.full-page-background');

				$input = $(this);

				if ($input.is(':checked')) {
					if ($sidebar_img_container.length != 0) {
						$sidebar_img_container.fadeIn('fast');
						$sidebar.attr('data-image', '#');
					}

					if ($full_page_background.length != 0) {
						$full_page_background.fadeIn('fast');
						$full_page.attr('data-image', '#');
					}

					background_image = true;
				} else {
					if ($sidebar_img_container.length != 0) {
						$sidebar.removeAttr('data-image');
						$sidebar_img_container.fadeOut('fast');
					}

					if ($full_page_background.length != 0) {
						$full_page.removeAttr('data-image', '#');
						$full_page_background.fadeOut('fast');
					}

					background_image = false;
				}
			});

			$('.switch-sidebar-mini input').change(function () {
				$body = $('body');

				$input = $(this);

				if (md.misc.sidebar_mini_active == true) {
					$('body').removeClass('sidebar-mini');
					md.misc.sidebar_mini_active = false;

					$('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

				} else {

					$('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

					setTimeout(function () {
						$('body').addClass('sidebar-mini');

						md.misc.sidebar_mini_active = true;
					}, 300);
				}

				// we simulate the window Resize so the charts will get updated in realtime.
				var simulateWindowResize = setInterval(function () {
					window.dispatchEvent(new Event('resize'));
				}, 180);

				// we stop the simulation of Window Resize after the animations are completed
				setTimeout(function () {
					clearInterval(simulateWindowResize);
				}, 1000);

			});
		});
	});
</script>
<script>
	$(document).ready(function () {
		// Javascript method's body can be found in assets/js/demos.js
		md.initDashboardPageCharts();

	});
</script>
<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
