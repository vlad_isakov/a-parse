<?php

use backend\controllers\ProxyController;
use yii\widgets\Pjax;

/**
 * @author Исаков Владислав
 *
 * @var \common\models\Proxy[] $proxyList
 * @var \common\models\Proxy[] $proxyBlockedList
 */
?>
<a class="btn btn-primary" href="<?= ProxyController::getActionUrl(ProxyController::ACTION_EDIT) ?>">Добавить</a>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header card-header-tabs card-header-primary">
				<div class="nav-tabs-navigation">
					<div class="nav-tabs-wrapper">
						<ul class="nav nav-tabs" data-tabs="tabs">
							<li class="nav-item">
								<a href="#active" class="nav-link active show" data-toggle="tab">Активные <span class="badge badge-light"><?= count($proxyList) ?></span></a>
							</li>
							<li class="nav-item">
								<a href="#blocked" class="nav-link" data-toggle="tab">Заблокированные <span class="badge badge-light"><?= count($proxyBlockedList) ?></span></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<?php Pjax::begin(['id' => 'proxy']); ?>
			<div class="card-body">
				<div class="tab-content">
					<div class="tab-pane active show" id="active">
						<table class="table table-bordered">
							<?php if (count($proxyList) === 0): ?>
								<tr>
									<td colspan="4" class="text-center">Серверов нет</td>
								</tr>
							<?php endif ?>
							<?php foreach ($proxyList as $proxy): ?>
								<tr>
									<td class="log-message">
										<?= $proxy->url ?>
									</td>
									<td class="text-center">
										<?= $proxy->insert_stamp ?>
									</td>
									<td class="text-center">
										<?= $proxy->update_stamp ?>
									</td>
									<td class="text-center" width="90px">
										<a href="<?= ProxyController::getActionUrl(ProxyController::ACTION_CHANGE_STATUS, ['id' => $proxy->id]) ?>"><i class="material-icons">block</i></a>
										<a href="<?= ProxyController::getActionUrl(ProxyController::ACTION_DELETE, ['id' => $proxy->id]) ?>"><i
													class="material-icons">clear</i></a>
									</td>

								</tr>
							<?php endforeach ?>
						</table>
					</div>
					<div class="tab-pane" id="blocked">
						<table class="table table-bordered">
							<?php if (false === $proxyBlockedList): ?>
								<tr>
									<td colspan="4" class="text-center">Заблокированных нет</td>
								</tr>
							<?php else: ?>
								<?php foreach ($proxyBlockedList as $proxy): ?>
									<tr>
										<td>
											<?= $proxy->url ?>
										</td>
										<td class="text-center">
											<?= $proxy->insert_stamp ?>
										</td>
										<td class="text-center">
											<?= $proxy->update_stamp ?>
										</td>
										<td class="text-center" width="10px">
											<a href="<?= ProxyController::getActionUrl(ProxyController::ACTION_CHANGE_STATUS, ['id' => $proxy->id]) ?>"><i class="material-icons">refresh</i></a>
										</td>
									</tr>
								<?php endforeach ?>
							<?php endif ?>
						</table>
					</div>
				</div>
			</div>
			<?php Pjax::end(); ?>
		</div>
	</div>
</div>
<?php $this->registerJs('$(this).proxyPlugin();'); ?>