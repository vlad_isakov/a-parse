<?php

use backend\controllers\LogController;
use common\models\LogYii;
use yii\widgets\Pjax;

/**
 * @author Исаков Владислав
 * @var \common\models\LogYii[] $logs
 * @var null|int                $status
 */
$this->params['breadcrumbs'][] = ['label' => 'Логи ошибок'];
?>
	<p><a href="<?= LogController::getActionUrl(LogController::ACTION_CLEAR) ?>" class="btn btn-danger">Очистить</a></p>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header card-header-tabs card-header-primary">
					<div class="nav-tabs-navigation">
						<div class="nav-tabs-wrapper">
							<ul class="nav nav-tabs">
								<li class="nav-item">
									<a href="<?= LogController::getActionUrl(LogController::ACTION_INDEX) ?>" class="nav-link <?= ($status === null ? 'active' : '') ?>">Все</a>
								</li>
								<li class="nav-item">
									<a href="<?= LogController::getActionUrl(LogController::ACTION_INDEX, ['status' => 1]) ?>" class="nav-link  <?= ($status == 1 ? 'active' : '') ?>">403</a>
								</li>
								<li class="nav-item">
									<a href="<?= LogController::getActionUrl(LogController::ACTION_INDEX, ['status' => 2]) ?>" class="nav-link <?= ($status == 2 ? 'active' : '') ?>">404</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<?php Pjax::begin(['id' => 'errors']); ?>
				<div class="card-body">
					<div class="tab-content">
						<div class="tab-pane active show">
							<table class="table table-bordered table-striped log table-hover">
								<tr>
									<th>Сообщение</th>
								</tr>
								<?php if (count($logs) === 0): ?>
									<tr>
										<td colspan="2" class="text-center">Ошибок нет</td>
									</tr>
								<?php endif ?>
								<?php foreach ($logs as $logRecord): ?>
									<tr>
										<td class="log-message">
											<p>
											<div class="alert alert-danger">
												<strong><?= $logRecord->category ?></strong>
												<span class="pull-right"><?= $logRecord->log_time ?></span>
											</div>
											</p>
											<p>
												<b><?= LogYii::SITE_NAMES[$logRecord->site_id] ?></b>
											</p>
											<p><?= $logRecord->hostname ?></p>
											<pre><?= $logRecord->message ?></pre>
										</td>
									</tr>
								<?php endforeach ?>
							</table>
						</div>
					</div>
				</div>
				<?php Pjax::end(); ?>
			</div>
		</div>
	</div>
<?php $this->registerJs('$(this).errorsPlugin();'); ?>