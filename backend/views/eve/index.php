<?php

use common\base\helpers\StringHelper;
use yii\bootstrap4\LinkPager;

/**
 * @author Исаков Владислав
 *
 * @var \backend\models\RouteSearchForm $form
 * @var \yii\data\Pagination           $pages
 */
?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header card-header-tabs card-header-primary">
				<div class="nav-tabs-navigation">
					<div class="nav-tabs-wrapper">
						Trade routes
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="tab-content">
					<div class="tab-pane active show" id="active">
						<?= LinkPager::widget([
							'pagination'     => $pages,
							'maxButtonCount' => 20
						]);
						?>
						<table class="table table-bordered table-hover" style="line-height: 1.4em; font-size: .675rem;">
							<tr>
								<th>
								</th>
								<th class="text-center">
									Item
								</th>
								<th>
									Route
								</th>
								<th class="text-center">
									Buy
								</th>
								<th class="text-center">
									Profit
								</th>
								<th class="text-center">
									Count
								</th>
								<th class="text-center">
									Volume
								</th>
                                <th class="text-center">
                                    Actual at
                                </th>
							</tr>
							<?php foreach ($form->routes as $route): ?>
								<?php
								$isLowSec = round($route->buy_security, 2) < 0;
								?>
								<tr>
									<td width="10px" class="alert-<?= ($route->sell_security < '0.5' || $isLowSec ? 'danger' : 'success') ?>">

									</td>
									<td class="text-center">
										<?= $route->name ?>
									</td>
									<td>
										<--(<?= round($route->sell_security, 2) ?>)<?= $route->from ?><br> -->
										(<?= round($route->buy_security, 2) ?>)<?= $route->to ?>
									</td>
									<td class="text-center">
										<?= StringHelper::formatPrice($route->sell_price * $route->count) ?> ISK
									</td>
									<td class="text-center">
										<?= StringHelper::formatPrice($route->profit) ?> ISK
									</td>
									<td class="text-center">
										<?= $route->count ?>
									</td>
									<td class="text-center">
										<?= StringHelper::formatPrice($route->count * $route->one_volume) ?> m<sup>3</sup>
									</td>
                                    <td class="text-center">
                                        <?= $route->insert_stamp ?>
                                    </td>
								</tr>
							<?php endforeach ?>
						</table>
					</div>
				</div>
				<?= LinkPager::widget([
					'pagination'     => $pages,
					'maxButtonCount' => 20
				]);
				?>
			</div>
		</div>
	</div>
</div>
