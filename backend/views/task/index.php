<?php

use backend\controllers\SiteController;
use backend\controllers\TaskController;
use common\models\Task;
use yii\helpers\HtmlPurifier;
use yii\widgets\Pjax;

/**
 * @author Исаков Владислав
 *
 * @var Task[]                      $tasks
 * @var null|int                    $status null
 * @var \common\models\TariffUser[] $userTariffs
 */
?>
<div class="renew-url" data-url="<?= TaskController::getActionUrl(TaskController::ACTION_RENEW) ?>">
<?php if (count($userTariffs) > 0): ?>
	<a class="btn btn-primary" href="<?= TaskController::getActionUrl(TaskController::ACTION_EDIT) ?>">Добавить</a>
<?php else: ?>
	<div class="alert alert-info">Для добавления задач парсинга необходимо активировать один из <strong><a href="<?= SiteController::getActionUrl(SiteController::ACTION_PROFILE) ?>">тарифных планов</a></strong></div>
<?php endif ?>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header card-header-tabs card-header-primary">
					<div class="nav-tabs-navigation">
						<div class="nav-tabs-wrapper">
							<ul class="nav nav-tabs">
								<li class="nav-item">
									<a href="<?= TaskController::getActionUrl(TaskController::ACTION_INDEX) ?>" class="nav-link <?= ($status === null ? 'active' : '') ?> ">Все</a>
								</li>
								<li class="nav-item">
									<a href="<?= TaskController::getActionUrl(TaskController::ACTION_INDEX, ['status' => Task::STATUS_IN_PROGRESS]) ?>" class="nav-link <?= ($status == Task::STATUS_IN_PROGRESS ? 'active' : '') ?>">Активные</a>
								</li>
								<li class="nav-item">
									<a href="<?= TaskController::getActionUrl(TaskController::ACTION_INDEX, ['status' => Task::STATUS_COMPLETE]) ?>" class="nav-link <?= ($status == Task::STATUS_COMPLETE ? 'active' : '') ?>">Завершенные</a>
								</li>
								<li class="nav-item">
									<a href="<?= TaskController::getActionUrl(TaskController::ACTION_INDEX, ['status' => Task::STATUS_ERROR]) ?>" class="nav-link <?= ($status == Task::STATUS_ERROR ? 'active' : '') ?>">Ошибки</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!--PJAX-->
				<?php Pjax::begin(['id' => 'tasks']); ?>
				<div class="card-body">
					<?php if (count($userTariffs) > 0): ?>
						<div class="tab-content">
							<div class="tab-pane active show">
								<table class="table table-hover">
									<thead class="thead-light">
									<tr>
										<th></th>
										<th></th>
										<th>Url</th>
										<th>Кол-во</th>
										<th>Дата</th>
										<th>Сатус</th>
										<th></th>
										<th></th>
									</tr>
									</thead>
									<?php foreach ($tasks as $task): ?>
										<tr>
											<td class="text-center <?= ($task->status === Task::STATUS_ERROR ? 'alert-danger' : 'alert-success') ?>">
												<?php if ($task->status !== Task::STATUS_IN_QUEUE): ?>
													<a href="javascript:" class="renew-link" data-task-id="<?= $task->id ?>"><i class="material-icons">autorenew</i></a>
												<?php endif ?>
											</td>
											<td class="text-center <?= ($task->status === Task::STATUS_ERROR ? 'alert-danger' : 'alert-success') ?>">
												<?php if ($task->status !== Task::STATUS_IN_QUEUE): ?>
													<a href="<?= TaskController::getActionUrl(TaskController::ACTION_EDIT, ['id' => $task->id]) ?>"><i class="material-icons">create</i></a>
												<?php endif ?>
											</td>
											<td>
												<?= HtmlPurifier::process($task->url) ?>
												<?php if ($task->is_repeat): ?>
													<span class="badge badge-success">Повторяется</span>
												<?php endif ?>
												<?php if ($task->is_send_to_email): ?>
													<span class="badge badge-success">Отправляется на почту</span>
												<?php endif ?>
											</td>
											<td><?= $task->count ?></td>
											<td><?= $task->update_stamp ?></td>
											<td><strong><?= Task::STATUS_NAMES[$task->status] ?></strong></td>
											<td>
												<?php foreach ($task->getFiles() as $file): ?>
													<a target="_blank" class="pull-right" href='<?= TaskController::getActionUrl(TaskController::ACTION_GET_FILE, ['fileName' => $file->filename]) ?>'>
														<img width="30px" src="<?= (strpos($file->filename, '.csv') ? '/assets/img/csv.png' : '/assets/img/json.png') ?>" alt="CSV">
													</a>
												<?php endforeach ?>
											</td>
											<td class="text-center alert-danger">
												<?php if ($task->status !== Task::STATUS_IN_QUEUE): ?>
													<a href="<?= TaskController::getActionUrl(TaskController::ACTION_DELETE, ['id' => $task->id]) ?>"><i
																class="material-icons">clear</i></a>
												<?php endif ?>
											</td>
										</tr>
										<tr>
											<td colspan="8" class="text-center">
												<?php if ($task->status === Task::STATUS_IN_PROGRESS): ?>
													<div class="progress">
														<div class="progress-bar" role="progressbar" style="width: <?= round(((int)$task->completed / (int)$task->count) * 100) ?>%;" aria-valuenow="<?= $task->completed ?>" aria-valuemin="0"
														     aria-valuemax="<?=
															 $task->count ?>"><?= $task->completed
															?></div>
													</div>
													<br>
												<?php endif ?>
											</td>
										</tr>
									<?php endforeach ?>
								</table>
							</div>
						</div>
					<?php endif ?>
				</div>
				<?php $this->registerJs('$(this).tasksActionsPlugin();'); ?>
				<?php Pjax::end(); ?>
				<!--PJAX END-->
			</div>
		</div>
	</div>
<?php $this->registerJs('$(this).tasksPlugin();'); ?>