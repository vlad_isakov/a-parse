<?php

use kartik\icons\FontAwesomeAsset;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

/**
 * @author Исаков Владислав
 *
 * @var \yii\web\View       $this
 * @var \common\models\Task $model
 */

FontAwesomeAsset::register($this);
?>
<div class="col-md-12">
	<div class="card">
		<div class="card-header card-header-success">
			<h4 class="card-title">
				Заполните необходимые данные
			</h4>
		</div>
		<div class="card-body">
			<br>
			<?php $htmlForm = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<?= $htmlForm->field($model, $model::ATTR_URL)->textInput() ?>
				</div>
				<div class="col-md-3 col-sm-12">
					<?= $htmlForm->field($model, $model::ATTR_COUNT)->textInput() ?>
				</div>
			</div>
			<?php if (1 == 1): //Тут проверить права по тарифу?>
				<div class="row">
					<div class="col-md-3 col-sm-12">
						<?= $htmlForm->field($model, $model::ATTR_IS_SEND_TO_EMAIL)->checkbox() ?>
					</div>
					<div class="col-md-3 col-sm-12">
						<?= $htmlForm->field($model, $model::ATTR_IS_REPEAT)->checkbox() ?>
					</div>
				</div>
			<?php endif ?>
			<div class="form-group">
				<?= Html::submitButton('Применить', ['class' => 'btn btn-success']) ?>
			</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
