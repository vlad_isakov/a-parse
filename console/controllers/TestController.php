<?php

namespace console\controllers;

use common\base\helpers\CurlHelper;
use common\base\helpers\ProxyHelper;
use common\components\entities\AvitoItem;
use common\models\EveTradeRoute;
use DiDom\Document;
use Exception;
use Yii;

/**
 * Тестовые команды
 *
 * @package app\commands
 *
 * @author  Исаков Владислав
 */
class TestController extends BaseController {

	/**
	 * Получение списка прокси-серверов
	 *
	 * @author Исаков Владислав
	 */
	public function actionRenewProxyList() {
		ProxyHelper::fillProxyList();
	}

	/**
	 * Парсинг Автио
	 *
	 * @author Исаков Владислав
	 */
	public function actionParseAvito() {
		$count = 50;
		$parseToJsonResult = [];
		$parseToCsvResult = [];
		for ($page = 0; $page < $count / 50; $page++) {
			echo 'Get page: ' . $page . PHP_EOL;
			flush();
			//$categoryUrl = 'https://www.avito.ru/vladivostok/noutbuki';
			//$categoryUrl = 'https://www.avito.ru/ufa/kvartiry/prodam-ASgBAgICAUSSA8YQ';
			$categoryUrl = 'https://www.avito.ru/ufa/kvartiry/prodam';
			sleep(1);
			if ($page > 0) {
				$categoryUrl .= '?p=' . $page;
			}

			$curl = curl_init($categoryUrl);
			$options = CurlHelper::getRandomOptions('https://www.avito.ru', true);
			curl_setopt_array($curl, $options);

			$result = curl_exec($curl);

			$info = curl_getinfo($curl);

			if ($info['url'] === 'https://www.avito.ru/blocked' || $info['size_download'] == 0) {
				curl_close($curl);
				ProxyHelper::blockProxy($options);
				die;
			}

			if (is_bool($result) || '' === $result) {
				print_r('no list: ' . $result);
				flush();

				continue;
			}

			curl_close($curl);

			$html = new Document();
			$html->loadHtml($result);

			$urls = [];
			$links = $html->find('.snippet-link');

			foreach ($links as $link) {
				$urls[] = 'https://www.avito.ru' . $link->getAttribute('href');
			}
			$linksCount = count($urls) - 1;
			$i = 0;
			foreach ($urls as $url) {
				echo 'Get Url: ' . $url . PHP_EOL;
				flush();

				usleep(rand(1000000, 1500000));
				$avitoItem = new AvitoItem();
				$avitoItem->categoryUrl = $categoryUrl;
				try {
					$avitoItem->getDataFromDom($url, $categoryUrl);
				}
				catch (Exception $e) {
					$i++;
					self::showStatus($i, $linksCount);
					usleep(10000);
					continue;
				}

				$parseToJsonResult[] = $avitoItem;
				//сконвертируем массивы для выгрузки в CSV
				$avitoItemCsv = clone $avitoItem;
				$csvParams = [];
				foreach ($avitoItemCsv->params as $param => $value) {
					$csvParams[] = $param . ': ' . $value;
				}

				$avitoItemCsv->images = implode(PHP_EOL, $avitoItemCsv->images);
				$avitoItemCsv->params = implode(PHP_EOL, $csvParams);
				unset($avitoItemCsv->categoryUrl);
				$parseToCsvResult[] = $avitoItemCsv;

				$i++;
				self::showStatus($i, $linksCount);
				usleep(10000);
			}
		}

		$fp = fopen(Yii::getAlias('@cookies') . DIRECTORY_SEPARATOR . time() . '_result.csv', 'w');
		fputcsv($fp, ['ID', 'Название', 'Цена', 'Характеристики', 'Телефон', 'Ссылка', 'Описание', 'Изображения', 'Адрес']);
		foreach ($parseToCsvResult as $fields) {
			fputcsv($fp, get_object_vars($fields));
		}
		fclose($fp);

		$xmlData = new SimpleXMLElement('<?xml version="1.0"?><data></data>');
		LArray::toXml($parseToJsonResult, $xmlData);
		$xmlData->asXML(Yii::getAlias('@cookies') . DIRECTORY_SEPARATOR . time() . '_result.xml');


		$parseToJsonResult = json_encode($parseToJsonResult);

		file_put_contents(Yii::getAlias('@cookies') . DIRECTORY_SEPARATOR . time() . '_result.json', $parseToJsonResult);
	}

	public function actionTestProxyList() {
		$proxyList = [
			'181.129.165.102:999',
			'124.158.177.171:23500',
			'103.240.161.109:6666',
			'191.189.30.85:54917',
			'163.53.198.54:42787',
			'203.150.128.29:8080',
			'165.73.128.173:56975',
			'180.242.48.204:8080',
			'202.21.98.150:60640',
			'96.113.183.214:3128',
		];

		foreach ($proxyList as $proxy) {
			ProxyHelper::checkProxy($proxy);
			echo PHP_EOL;
			flush();
		}
	}

	/**
	 * Тест парсинга торговых маршрутов EVE
	 *
	 * @author Исаков Владислав
	 */
	public function actionTestEve() {
		$baseUrl = 'https://evemarketer.com/api/v1/markets/types/%s?language=ru&important_names=false';

        Yii::$app->db->createCommand()->truncateTable('eve_trade_route')->execute();

		$itemIds = [
			'46001',
			'46002',
			'46003',
			'46004',
			'46005',
			'46006',
			'48114',
			'33525',
			'31975',
			'53711',
			'20501',
			'19551',
			'11283',
			'11285',
			'11287',
			'11289',
			'41489',
			'31982',
			'11283',
			'31998',
			'32014',
			'28268',
			'28288',
			'28292',
			'28308',
			'28266',
			'28286',
			'28290',
			'28306',
			'31876',
			'31884',
			'33681',
			'31892',
			'28272',
			'28284',
			'28296',
			'28296',
			'28300',
			'28270',
			'28282',
			'28294',
			'28298',
			'31882',
			'31874',
			'31866',
			'31890',
			'16068',
			'8089',
			'13925',
			'13926',
			'8105',
			'16064',
			'13921',
			'13922',
			'17487',
			'15818',
			'15814',
			'13876',
			'13879',
			'19215',
			'19187',
			'19147',
			'19213',
			'19211',
			'4349',
			'19209',
			'19231',
			'19191',
			'19151',
			'19229',
			'19227',
			'4347',
			'19225',
			'10155',
			'15480',
			'25237',
			'32248',
			'46233',
            '28670',
            '10156',
            '9950',
            '16240',
            '4310',
			//---------------------------Минералы
			//'39',
			//'48916',
			//'37',
			//'36',
			//'11399',
			//'38',
			//'35',
			//'34',
			//---------------------------------
			//-----------------------Планетарка Высокотехнологичная
			'2867',
			'2868',
			'2869',
			'2870',
			'2871',
			'2872',
			'2875',
			'2876',
			//----------------------Планетарка Обработанная
			'2393',
			'2396',
			'3779',
			'2401',
			'2390',
			'2390',
			'2397',
			'2392',
			//'3683', Oxygen
			'2389',
			'2399',
			'2395',
			'2398',
			'9828',
			'2400',
			'3645',
            //-------------------------------------------Нркт
            '3707',
            '3705',
            '3824',
            '3709',
            '3818',
            '3822',
            '3826',
            '3703',
            '3820',
            '3713',
            '3711',
            //------------------------------------------Sleepers
            '30746',
            '30744',
            '30745',
            '30747',
			//-------------------------------------------Торпеды/ракеты
            '30488',
            '27447',
            '27435',
            '27453',
            '27441',
            '2811',
            '24523',
            '24519',
            '24519',
            //------------------------------------------Модули
            '4403',
            '33101',
            '3540',
            '3538',
            '2281',
            '578',
            //----------------------Майнинг
            '24305',
            '17912',
            '482',
            '483',
            '25812',
            '25266'
			//---------------------------Планетарка необработанная



		];
		$i = 1;
		foreach ($itemIds as $itemId) {
			self::showStatus($i, count($itemIds));
			$url = sprintf($baseUrl, $itemId);

			$curl = curl_init($url);
			$options = CurlHelper::getRandomOptions('https://evemarketer.com', false);
			curl_setopt_array($curl, $options);
			$result = curl_exec($curl);

			$result = json_decode($result);

			if (!isset($result->buy) || !isset($result->sell)) {
				continue;
			}

			$buy = $result->buy;
			$sell = $result->sell;

			$name = $result->type->name;
			$oneVolume = $result->type->volume;
			foreach ($sell as $sellItem) {
				foreach ($buy as $buyItem) {

					if ($sellItem->price >= $buyItem->price) {
						continue;
					}

					$priceDiff = $buyItem->price - $sellItem->price;

					$tradeRoute = new EveTradeRoute();
					$tradeRoute->name = $name;
					$tradeRoute->one_volume = $oneVolume;
					$tradeRoute->sell_price = $sellItem->price;
					$tradeRoute->buy_price = $buyItem->price;

					if ($sellItem->volume_remain > $buyItem->volume_remain) {
						$tradeRoute->count = $buyItem->volume_remain;
					}
					else {
						$tradeRoute->count = $sellItem->volume_remain;
					}

					$tradeRoute->sell_security = $sellItem->station->security;
					$tradeRoute->buy_security = $buyItem->station->security;
					$tradeRoute->from = $sellItem->station->name;
					$tradeRoute->to = $buyItem->station->name;
					$tradeRoute->profit = $priceDiff * $tradeRoute->count;

					if ((int)$tradeRoute->profit < 3000000) {
						continue;
					}

					$tradeRoute->save();
				}
			}
			sleep(1);
			$i++;
		}

	}

	/**
	 * Отправка curl-запроса на сайт
	 *
	 * @author Исаков Владислав
	 */
	public function actionSendRequest() {

		$curl = curl_init('http://t-gate.loc');
		$options = CurlHelper::getRandomOptions('http://t-gate.loc', true);
		curl_setopt_array($curl, $options);

		$result = curl_exec($curl);

		print_r($result);
		curl_close($curl);
	}
}
