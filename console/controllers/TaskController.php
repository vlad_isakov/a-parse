<?php
/**
 * @author Исаков Владислав
 */

namespace console\controllers;


use common\models\Tariff;
use common\models\Task;

class TaskController extends BaseController {

	/**
	 * Запуск заданий
	 *
	 * @author Исаков Владислав
	 */
	public function actionRunTasks() {

		/** @var Task[] $tasksAvito */
		$tasksAvito = Task::find()
			->andWhere([Task::ATTR_STATUS => Task::STATUS_IN_QUEUE])
			->andWhere([Task::ATTR_SOURCE_ID => Tariff::SOURCE_AVITO])
			->all();

		foreach ($tasksAvito as $task) {
			$task->status = Task::STATUS_IN_PROGRESS;
			$task->save();

			$task->processAvito();
		}

	}


}