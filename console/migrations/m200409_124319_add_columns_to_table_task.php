<?php

use common\components\Migration;
use yii\db\mysql\Schema;

class m200409_124319_add_columns_to_table_task extends Migration {
	private $_tableName = 'task';

	public function safeUp() {
		$this->addColumn($this->_tableName, 'is_repeat', Schema::TYPE_BOOLEAN . ' DEFAULT FALSE');
		$this->addColumn($this->_tableName, 'is_send_to_email', Schema::TYPE_BOOLEAN . ' DEFAULT FALSE');
		$this->addColumn($this->_tableName, 'repeat_stamp', Schema::TYPE_DATETIME . ' DEFAULT NULL');
	}

	public function safeDown() {
		$this->dropColumn($this->_tableName, 'is_repeat');
		$this->dropColumn($this->_tableName, 'is_send_to_email');
		$this->dropColumn($this->_tableName, 'repeat_stamp');
	}
}
