<?php

use common\components\Migration;
use yii\db\mysql\Schema;

class m200409_124318_add_table_tarif extends Migration {
	private $_tableName = 'tariff';

	public function safeUp() {
		$this->createTable($this->_tableName, [
				'id'           => Schema::TYPE_PK,
				'title'        => Schema::TYPE_STRING . ' NOT NULL',
				'slug'         => Schema::TYPE_STRING . ' NOT NULL',
				'description'  => Schema::TYPE_TEXT . ' DEFAULT NULL',
				'active'       => Schema::TYPE_SMALLINT . ' DEFAULT 0',
				'cost'         => Schema::TYPE_INTEGER . ' NOT NULL',
				'count'        => Schema::TYPE_INTEGER . ' DEFAULT NULL',
				'insert_stamp' => Schema::TYPE_DATETIME . ' NOT NULL',
				'update_stamp' => Schema::TYPE_DATETIME . ' NOT NULL',
				'insert_user'  => Schema::TYPE_INTEGER . ' NOT NULL',
				'update_user'  => Schema::TYPE_INTEGER . ' NOT NULL'
			]
		);
	}

	public function safeDown() {
		$this->dropTable($this->_tableName);
	}
}
