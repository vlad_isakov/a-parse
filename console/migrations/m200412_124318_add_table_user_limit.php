<?php

use common\components\Migration;
use yii\db\mysql\Schema;

class m200412_124318_add_table_user_limit extends Migration {
	private $_tableName = 'user_limit';

	public function safeUp() {
		$this->createTable($this->_tableName, [
				'id'         => Schema::TYPE_PK,
				'user_id'    => Schema::TYPE_INTEGER . ' NOT NULL',
				'source_id'  => Schema::TYPE_SMALLINT . ' NOT NULL',
				'date_stamp' => Schema::TYPE_DATE . ' NOT NULL',
				'count'      => Schema::TYPE_INTEGER . ' NOT NULL',
			]
		);

		$this->createIndex(null, $this->_tableName, ['user_id', 'source_id']);
	}

	public function safeDown() {
		$this->dropTable($this->_tableName);
	}
}
