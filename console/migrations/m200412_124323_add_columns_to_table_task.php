<?php

use common\components\Migration;
use yii\db\mysql\Schema;

class m200412_124323_add_columns_to_table_task extends Migration {
	private $_tableName = 'task';

	public function safeUp() {
		$this->addColumn($this->_tableName, 'source_id', Schema::TYPE_SMALLINT . ' DEFAULT 0');
	}

	public function safeDown() {
		$this->dropColumn($this->_tableName, 'source_id');
	}
}
