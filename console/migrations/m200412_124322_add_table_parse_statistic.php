<?php

use common\components\Migration;
use yii\db\mysql\Schema;

class m200412_124322_add_table_parse_statistic extends Migration {
	private $_tableName = 'parse_statistic';

	public function safeUp() {
		$this->createTable($this->_tableName, [
				'id'           => Schema::TYPE_PK,
				'user_id'      => Schema::TYPE_INTEGER . ' NOT NULL',
				'url'          => Schema::TYPE_STRING . ' NOT NULL',
				'source_id'    => Schema::TYPE_SMALLINT . ' NOT NULL',
				'count'        => Schema::TYPE_INTEGER . ' DEFAULT 0',
				'insert_stamp' => Schema::TYPE_DATETIME . ' NOT NULL',
				'update_stamp' => Schema::TYPE_DATETIME . ' NOT NULL',
				'insert_user'  => Schema::TYPE_INTEGER . ' NOT NULL',
				'update_user'  => Schema::TYPE_INTEGER . ' NOT NULL'
			]
		);

		$this->createIndex(null, $this->_tableName, ['source_id']);
		$this->createIndex(null, $this->_tableName, ['user_id']);
	}

	public function safeDown() {
		$this->dropTable($this->_tableName);
	}
}
