<?php

use common\components\Migration;
use yii\db\pgsql\Schema;

class m200414_124322_add_table_proxy extends Migration {
	private $_tableName = 'proxy';

	public function safeUp() {
		$this->createTable($this->_tableName, [
				'id'           => Schema::TYPE_PK,
				'url'          => Schema::TYPE_STRING . ' NOT NULL',
				'active'       => Schema::TYPE_BOOLEAN . ' DEFAULT FALSE',
				'delay'        => Schema::TYPE_INTEGER . ' DEFAULT NULL',
				'insert_stamp' => Schema::TYPE_DATETIME . ' NOT NULL',
				'update_stamp' => Schema::TYPE_DATETIME . ' NOT NULL',
				'insert_user'  => Schema::TYPE_INTEGER . ' NOT NULL',
				'update_user'  => Schema::TYPE_INTEGER . ' NOT NULL'
			]
		);
	}

	public function safeDown() {
		$this->dropTable($this->_tableName);
	}
}
