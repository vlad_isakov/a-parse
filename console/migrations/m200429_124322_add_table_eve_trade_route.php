<?php

use common\components\Migration;
use yii\db\pgsql\Schema;

class m200429_124322_add_table_eve_trade_route extends Migration {
	private $_tableName = 'eve_trade_route';

	public function safeUp() {
		$this->createTable($this->_tableName, [
				'id'            => Schema::TYPE_PK,
				'name'          => Schema::TYPE_STRING . ' NOT NULL',
				'one_volume'    => Schema::TYPE_STRING . ' NOT NULL',
				'from'          => Schema::TYPE_STRING . ' NOT NULL',
				'to'            => Schema::TYPE_STRING . ' NOT NULL',
				'sell_price'    => Schema::TYPE_BIGINT . ' NOT NULL',
				'buy_price'     => Schema::TYPE_BIGINT . ' NOT NULL',
				'sell_security' => Schema::TYPE_STRING . ' NOT NULL',
				'buy_security'  => Schema::TYPE_STRING . ' NOT NULL',
				'count'         => Schema::TYPE_BIGINT . ' NOT NULL',
				'profit'        => Schema::TYPE_BIGINT . ' NOT NULL',
				'insert_stamp'  => Schema::TYPE_DATETIME . ' NOT NULL',
				'update_stamp'  => Schema::TYPE_DATETIME . ' NOT NULL',
				'insert_user'   => Schema::TYPE_INTEGER . ' NOT NULL',
				'update_user'   => Schema::TYPE_INTEGER . ' NOT NULL'
			]
		);
	}

	public function safeDown() {
		$this->dropTable($this->_tableName);
	}
}
