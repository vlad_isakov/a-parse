<?php

use common\components\Migration;
use yii\db\mysql\Schema;

class m281219_124318_add_table_place extends Migration {
	private $_tableName = 'place';

	public function safeUp() {
		$this->createTable($this->_tableName, [
				'id'                  => Schema::TYPE_PK,
				'title'               => Schema::TYPE_STRING . ' NOT NULL',
				'slug'               => Schema::TYPE_STRING . ' NOT NULL',
				'city_id'             => Schema::TYPE_BIGINT . ' NOT NULL',
				'nearest_location_id' => Schema::TYPE_BIGINT . ' NOT NULL',
				'lat'                 => Schema::TYPE_FLOAT . ' NOT NULL',
				'lon'                 => Schema::TYPE_FLOAT . ' NOT NULL',
				'description'         => Schema::TYPE_TEXT . ' NOT NULL',
				'tags'                => 'varchar(30)[] NOT NULL',
				'phones'              => 'varchar(11)[] NOT NULL',
				'emails'              => 'varchar(30)[] NOT NULL',
				'twitter'             => Schema::TYPE_STRING . ' DEFAULT NULL',
				'instagram'           => Schema::TYPE_STRING . ' DEFAULT NULL',
				'web_site'            => Schema::TYPE_STRING . ' DEFAULT NULL',
				'address'             => Schema::TYPE_STRING . ' DEFAULT NULL',
				'cover_image'         => Schema::TYPE_STRING,
				'rating'              => Schema::TYPE_SMALLINT . ' DEFAULT 0',
				'type'                => Schema::TYPE_SMALLINT . ' NOT NULL',
				'insert_stamp'        => Schema::TYPE_DATETIME . ' NOT NULL',
				'update_stamp'        => Schema::TYPE_DATETIME . ' NOT NULL',
				'insert_user'         => Schema::TYPE_INTEGER . ' NOT NULL',
				'update_user'         => Schema::TYPE_INTEGER . ' NOT NULL'
			]
		);

		$this->createIndex(null, $this->_tableName, ['city_id']);
		$this->createIndex(null, $this->_tableName, ['slug']);
	}

	public function safeDown() {
		$this->dropTable($this->_tableName);
	}
}
