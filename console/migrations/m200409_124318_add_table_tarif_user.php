<?php

use common\components\Migration;
use yii\db\mysql\Schema;

class m200409_124318_add_table_tarif_user extends Migration {
	private $_tableName = 'tariff_user';

	public function safeUp() {
		$this->createTable($this->_tableName, [
				'id'           => Schema::TYPE_PK,
				'user_id'      => Schema::TYPE_INTEGER . ' NOT NULL',
				'tariff_id'    => Schema::TYPE_INTEGER . ' NOT NULL',
				'begin_stamp'  => Schema::TYPE_DATETIME . ' NOT NULL',
				'end_stamp'    => Schema::TYPE_DATETIME . ' NOT NULL',
				'insert_stamp' => Schema::TYPE_DATETIME . ' NOT NULL',
				'update_stamp' => Schema::TYPE_DATETIME . ' NOT NULL',
				'insert_user'  => Schema::TYPE_INTEGER . ' NOT NULL',
				'update_user'  => Schema::TYPE_INTEGER . ' NOT NULL'
			]
		);

		$this->createIndex(null, $this->_tableName, ['user_id', 'tariff_id']);
		$this->createIndex(null, $this->_tableName, ['begin_stamp', 'end_stamp']);
	}

	public function safeDown() {
		$this->dropTable($this->_tableName);
	}
}
