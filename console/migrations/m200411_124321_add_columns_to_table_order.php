<?php

use common\components\Migration;
use yii\db\mysql\Schema;

class m200411_124321_add_columns_to_table_order extends Migration {
	private $_tableName = 'order';

	public function safeUp() {
		$this->addColumn($this->_tableName, 'kassa_id', Schema::TYPE_STRING . ' DEFAULT NULL');
	}

	public function safeDown() {
		$this->dropColumn($this->_tableName, 'kassa_id');
	}
}
