<?php

use common\components\Migration;
use yii\db\mysql\Schema;

class m200405_124318_add_table_task extends Migration {
	private $_tableName = 'task';

	public function safeUp() {
		$this->createTable($this->_tableName, [
				'id'           => Schema::TYPE_PK,
				'url'          => Schema::TYPE_STRING . ' NOT NULL',
				'status'       => Schema::TYPE_SMALLINT . ' DEFAULT 0',
				'completed'    => Schema::TYPE_SMALLINT . ' DEFAULT 0',
				'count'        => Schema::TYPE_INTEGER . ' NOT NULL',
				'insert_stamp' => Schema::TYPE_DATETIME . ' NOT NULL',
				'update_stamp' => Schema::TYPE_DATETIME . ' NOT NULL',
				'insert_user'  => Schema::TYPE_INTEGER . ' NOT NULL',
				'update_user'  => Schema::TYPE_INTEGER . ' NOT NULL'
			]
		);

		$this->createIndex(null, $this->_tableName, ['insert_user']);
		$this->createIndex(null, $this->_tableName, ['insert_user', 'status']);
	}

	public function safeDown() {
		$this->dropTable($this->_tableName);
	}
}
