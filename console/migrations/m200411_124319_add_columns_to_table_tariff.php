<?php

use common\components\Migration;
use yii\db\mysql\Schema;

class m200411_124319_add_columns_to_table_tariff extends Migration {
	private $_tableName = 'tariff';

	public function safeUp() {
		$this->addColumn($this->_tableName, 'may_is_phone', Schema::TYPE_BOOLEAN . ' DEFAULT FALSE');
		$this->addColumn($this->_tableName, 'may_is_repeat', Schema::TYPE_BOOLEAN . ' DEFAULT FALSE');
		$this->addColumn($this->_tableName, 'may_is_send_to_email', Schema::TYPE_BOOLEAN . ' DEFAULT FALSE');
	}

	public function safeDown() {
		$this->dropColumn($this->_tableName, 'may_is_phone');
		$this->dropColumn($this->_tableName, 'may_is_repeat');
		$this->dropColumn($this->_tableName, 'may_is_send_to_email');
	}
}
