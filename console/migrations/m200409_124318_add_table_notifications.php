<?php

use common\components\Migration;
use yii\db\mysql\Schema;

class m200409_124318_add_table_notifications extends Migration {
	private $_tableName = 'notification';

	public function safeUp() {
		$this->createTable($this->_tableName, [
				'id'           => Schema::TYPE_PK,
				'type'         => Schema::TYPE_SMALLINT . ' NOT NULL',
				'user_id'      => Schema::TYPE_INTEGER . ' NOT NULL',
				'insert_stamp' => Schema::TYPE_DATETIME . ' NOT NULL',
				'update_stamp' => Schema::TYPE_DATETIME . ' NOT NULL',
				'insert_user'  => Schema::TYPE_INTEGER . ' NOT NULL',
				'update_user'  => Schema::TYPE_INTEGER . ' NOT NULL'
			]
		);

		$this->createIndex(null, $this->_tableName, ['user_id']);
	}

	public function safeDown() {
		$this->dropTable($this->_tableName);
	}
}
