<?php

use common\components\Migration;
use yii\db\mysql\Schema;

class m200409_124318_add_table_order extends Migration {
	private $_tableName = 'order';

	public function safeUp() {
		$this->createTable($this->_tableName, [
				'id'            => Schema::TYPE_PK,
				'number'        => Schema::TYPE_STRING . ' DEFAULT NULL',
				'status'        => Schema::TYPE_SMALLINT . ' NOT NULL',
				'user_id'       => Schema::TYPE_INTEGER . ' NOT NULL',
				'tariff_id'     => Schema::TYPE_INTEGER . ' NOT NULL',
				'payment_stamp' => Schema::TYPE_DATETIME . ' DEFAULT NULL',
				'insert_stamp'  => Schema::TYPE_DATETIME . ' NOT NULL',
				'update_stamp'  => Schema::TYPE_DATETIME . ' NOT NULL',
				'insert_user'   => Schema::TYPE_INTEGER . ' NOT NULL',
				'update_user'   => Schema::TYPE_INTEGER . ' NOT NULL'
			]
		);

		$this->createIndex(null, $this->_tableName, ['user_id']);
		$this->createIndex(null, $this->_tableName, ['tariff_id']);
	}

	public function safeDown() {
		$this->dropTable($this->_tableName);
	}
}
