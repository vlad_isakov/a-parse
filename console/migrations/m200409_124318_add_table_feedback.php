<?php

use common\components\Migration;
use yii\db\mysql\Schema;

class m200409_124318_add_table_feedback extends Migration {
	private $_tableName = 'feedback';

	public function safeUp() {
		$this->createTable($this->_tableName, [
				'id'           => Schema::TYPE_PK,
				'name'         => Schema::TYPE_STRING . ' NOT NULL',
				'email'        => Schema::TYPE_STRING . ' NOT NULL',
				'text'         => Schema::TYPE_STRING . ' NOT NULL',
				'parent_id'    => Schema::TYPE_INTEGER . ' DEFAULT NULL',
				'insert_stamp' => Schema::TYPE_DATETIME . ' NOT NULL',
				'update_stamp' => Schema::TYPE_DATETIME . ' NOT NULL',
				'insert_user'  => Schema::TYPE_INTEGER . ' NOT NULL',
				'update_user'  => Schema::TYPE_INTEGER . ' NOT NULL'
			]
		);

		$this->createIndex(null, $this->_tableName, ['insert_user']);
		$this->createIndex(null, $this->_tableName, ['parent_id']);
	}

	public function safeDown() {
		$this->dropTable($this->_tableName);
	}
}
