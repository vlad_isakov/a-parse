/* jshint node: true */

const mix = require('laravel-mix');

mix.js('backend/assets/src/js/tasks.js', 'backend/web/js/tgate-backend.js');
mix.js('backend/assets/src/js/proxy.js', 'backend/web/js/tgate-backend.js');
mix.js('backend/assets/src/js/errors.js', 'backend/web/js/tgate-backend.js');
mix.js('backend/assets/src/js/tasksActions.js', 'backend/web/js/tgate-backend.js')
	.version();

mix.sass('backend/assets/src/sass/common.scss', 'backend/web/css')
	.sass('backend/assets/src/sass/material-dashboard.scss', 'backend/web/css')
	.options({
		imgLoaderOptions: {enabled: false},
		postCss: [
			require('postcss-css-variables')()
		]
	});

mix.styles([
	'backend/web/css/material-dashboard.css',
	'backend/web/css/common.css',
	'backend/assets/src/css/site.css',
], 'backend/web/css/site-backend.css')
	.version();

mix.setPublicPath('backend/web/');