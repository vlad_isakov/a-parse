const mix = require('laravel-mix');

mix.js('frontend/assets/src/js/commonPlugin.js', 'frontend/web/js/516.js')
	.version();

mix.sass('frontend/assets/src/sass/common.scss', 'frontend/web/css')
	.options({
		imgLoaderOptions: {enabled: false},
		postCss: [
			require('postcss-css-variables')()
		]
	});

mix.setPublicPath('frontend/web/');
